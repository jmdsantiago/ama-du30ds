<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Duterte Supporters - Advocacy</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
	<link href="css/main.css" rel="stylesheet">
	<link href="css/animate.css" rel="stylesheet">	
	<link href="css/responsive.css" rel="stylesheet">

    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body>
	<header id="header" role="banner">		
		<div class="main-nav">
			<div class="container">
				<div class="header-top">
				<!-- 	<div class="pull-right social-icons">
						<a href="#"><i class="fa fa-twitter"></i></a>
						<a href="#"><i class="fa fa-facebook"></i></a>
						<a href="#"><i class="fa fa-google-plus"></i></a>
						<a href="#"><i class="fa fa-youtube"></i></a>
					</div> --> 
				</div>   
		        <div class="row">	        		
		            <div class="navbar-header">
		                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
		                    <span class="sr-only">Toggle navigation</span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                    <span class="icon-bar"></span>
		                </button>
		                <a class="navbar-brand" href="index.php">
						<h1>Du30 - Development and Security</h1>
		                	<!-- <img class="img-responsive" src="images/logos.png" alt="logo"> -->
		                </a>                    
		            </div>
		            <div class="collapse navbar-collapse">
		                <ul class="nav navbar-nav navbar-right">                 
		                    <li class="scroll active"><a href="#home">Home</a></li>
		                    <li class="scroll"><a href="#explore">Advocacy</a></li>                         
		                    <li class="scroll"><a href="#event">Sponsors</a></li> 
							<li class="scroll"><a href="#about">Be a Sponsor</a></li>      
		                </ul>
		            </div>
		        </div>
	        </div>
        </div>                    
    </header>
    <!--/#header--> 

    <section id="home">	
		<div id="main-slider" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#main-slider" data-slide-to="0" class="active"></li>
				<li data-target="#main-slider" data-slide-to="1"></li>
				<li data-target="#main-slider" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="item active">
					<img class="img-responsive" src="images/slider/1.jpg" alt="slider">						
					
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/2.jpg" alt="slider">	
					
				</div>
				<div class="item">
					<img class="img-responsive" src="images/slider/3.jpg" alt="slider">	
					
				</div>				
			</div>
		</div>    	
    </section>
	<!--/#home-->

	<section id="explore">
		<div class="container">
			<div class="row">			
				<div class="col-md-12">
                                        <h2>ADVOCACY</h2>
				</div>				
				<div class="col-md-12">
                                <p><b>FEDERALISM: THE ONLY ALTERNATIVE</b> <small>by: Peter Lavina</small><br><br>
                                    <i>"..Federalism simply means giving more powers and resources to the regions or states to end the grip of too much control in a 
                                    highly-centralized unitary system. The center or capital should not have the monopolistic role in nation-building. All regions, 
                                    including the Bangsamoro, must be given their fair share of tasks and resources for our country's quest for peace and development."
                                </i></p><small>Source: davaocinto.wordpress.com/2015/02/16/federalism-the-only-alternative/</small>
				</div>
				
			</div>
		</div>
	</section><!--/#explore-->

	<section id="event">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div id="event-carousel" class="carousel slide" data-interval="false">
						<h2 class="heading">Sponsors</h2>
						<div class="carousel-inner">
							<div class="item active">
								<div class="row">
									<div id="sponsor_data">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</section><!--/#event-->

	<section id="about">
	<div class="container">
		<div class="row">
		<div class="col-md-4">
		</div>
			<div class="col-md-4">					
			<h2>Be a Sponsor</h2>
			<form role="form" id="the_form" enctype="multipart/form-data" >
            	<div class="form">
                    <label>Personal Information *</label>
					<div class="form-group" >
                	<input id="the_fname" class="form-control" type="text" name="" placeholder="First Name" required>
					</div>
					<div class="form-group" >
                    <input id="the_mname" class="form-control" type="text" name="" placeholder="Middle Name" required>
					</div>
					<div class="form-group" >
                    <input id="the_lname" class="form-control" type="text" name="" placeholder="Last  Name" required>
					</div>
                    <label>Gender *</label>
					<div class="form-group" >			
					<select id="gender" class="form-control">
					<option>Male</option>
					<option>Female</option>
					</select>
                    </div>
					<label>Contact *</label>
					<div class="form-group" >
                    <input id="the_contact" class="form-control" type="text" name="" placeholder="Contact" required>
					</div>
					<label>Email Address *</label>
					<div class="form-group" >
                    <input id="the_email" class="form-control" type="email" name="" placeholder="E-mail Address" required>
					</div>
                	<label>Address *</label>
					<div class="form-group" >
					<textarea id="the_address" class="form-control text-area" cols="0" rows="0" placeholder="Address" required></textarea>
					</div>
                    <label>Company Name *</label>
					<div class="form-group" >
					<input id="the_company" class="form-control" type="text" name="" placeholder="Company Name" required>
					</div>
					<label>Company Logo *</label>
					<div class="form-group" >
                    <input class="form-control" type="file" name="myfile" id="the_logo" required>
					</div>
					<div class="form-group" >
                    <input id="the_register" class="btn btn-primary" type="submit" value="Register">
					</div>
                </div>	
			</form>
			
			</div>
		
		</div>
	</div>
	</section><!--/#about-->
	

    <footer id="footer">
        <div class="container">
            <div class="text-center">
                <p> Copyright  &copy;2016<a href="http://www.du30ds.ph"> Duterte - Development and Security</a></p>                
            </div>
        </div>
    </footer>
    <!--/#footer-->
  
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
  	<script type="text/javascript" src="js/gmaps.js"></script>
	<script type="text/javascript" src="js/smoothscroll.js"></script>
    <script type="text/javascript" src="js/jquery.parallax.js"></script>
    <script type="text/javascript" src="js/coundown-timer.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.js"></script>
    <script type="text/javascript" src="js/jquery.nav.js"></script>
    <script type="text/javascript" src="js/main.js"></script>  
	<script type="text/javascript" src="js/jquery.numeric.min.js"></script>
	<script type="text/javascript">
		$("#the_contact").numeric();
		$("#sponsor_data").load("php/sponsor-data.php");
		
		$("#the_register").on('click', function(e){
		var myForm = $('#the_form');
		if (!myForm[0].checkValidity()) {
		}else{
		var fname = $("#the_fname").val();
		var mname = $("#the_mname").val();
		var lname = $("#the_lname").val();
		var email = $("#the_email").val();
		var address = $("#the_address").val();
		var gender = $("#gender").val();
		var contact = $("#the_contact").val();
		var company = $("#the_company").val();
		var filename = $("#the_logo")[0].files[0]["name"];
		var the_data = "fname="+fname+"&mname="+mname+"&lname="+lname+"&email="+email+"&address="+address+"&gender="+gender+"&contact="+contact+"&company="+company+"&logo="+filename; 
	
			$.ajax({
			type: "POST",
			url: "php/add-supporter.php",
			data: the_data,
			success: function(html){
			alert(html);
			$("#the_fname").val("");
			$("#the_mname").val("");
			$("#the_lname").val("");
			$("#the_region").val("");
			$("#the_city_list").val("");
			$("#the_email").val("");
			$("#the_address").val("");
			$("#the_month").val("");
			$("#the_day").val("");
			$("#the_year").val("");
			$("#the_sector").val("");
			$("#the_contact").val("");
			$("#the_city").val("");
			$("#the_company").val("");
			$("#sponsor_data").load("php/sponsor-data.php");
			}
			});
			return false;
			}
		});
		
			
			//DRI ANG IEDIT DOY!!!	
			$('#the_register').on('click', function() {
			var file_data = $('#the_logo').prop('files')[0];   
			var form_data = new FormData();                  
			form_data.append('file', file_data);                          
			$.ajax({
                url: 'upload.php', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post'
			});
			});
	</script>
</body>
</html>				