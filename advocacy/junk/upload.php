<?php
/*
    if ( 0 < $_FILES['file']['error'] ) {
        echo 'Error: ' . $_FILES['file']['error'] . '<br>';
    }
    else {
        move_uploaded_file($_FILES['file']['tmp_name'], 'uploaded/' . $_FILES['file']['name']);
    }
*/
?>

<?php
// include ImageManipulator class
require_once('php/ImageManipulator.php');
 
if ($_FILES['file']['error'] > 0) {
    echo "Error: " . $_FILES['file']['error'] . "<br />";
} else {
    // array of valid extensions
    $validExtensions = array('.jpg', '.jpeg', '.gif', '.png');
    // get extension of the uploaded file
    $fileExtension = strrchr($_FILES['file']['name'], ".");
    // check if file Extension is on the list of allowed ones
    if (in_array($fileExtension, $validExtensions)) {
		$newNamePrefix = time() . '_';
        $manipulator = new ImageManipulator($_FILES['file']['tmp_name']);
        // resizing to 200x200
        $newImage = $manipulator->resample(250, 250);
        // saving file to uploaded folder
        $manipulator->save('uploaded/' . $newNamePrefix . $_FILES['file']['name']);
        echo 'Done ...';
    } else {
        echo 'You must upload an image...';
    }
}
?>