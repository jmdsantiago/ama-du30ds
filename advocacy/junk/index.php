<?php
include("php/connection.php");
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, maximum-scale=1">

<title>Duterte for Development and Security</title>
<link rel="icon" href="favicon.png" type="image/png">
<link rel="shortcut icon" href="favicon.ico" type="img/x-icon">

<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,800italic,700italic,600italic,400italic,300italic,800,700,600' rel='stylesheet' type='text/css'>

<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/font-awesome.css" rel="stylesheet" type="text/css">
<link href="css/responsive.css" rel="stylesheet" type="text/css">
<link href="css/animate.css" rel="stylesheet" type="text/css">

<!--[if IE]><style type="text/css">.pie {behavior:url(PIE.htc);}</style><![endif]-->

<script type="text/javascript" src="js/jquery.1.8.3.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/jquery-scrolltofixed.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.isotope.js"></script>
<script type="text/javascript" src="js/wow.js"></script>
<script type="text/javascript" src="js/classie.js"></script>
<script type="text/javascript" src="js/jquery.numeric.min.js"></script>

<!--[if lt IE 9]>
    <script src="js/respond-1.1.0.min.js"></script>
    <script src="js/html5shiv.js"></script>
    <script src="js/html5element.js"></script>
<![endif]-->


</head>
<style>
.ghost{
  display: inline-block;
  padding: 8px;
  color: #fff;
  background-color: transparent;
  border: 2px solid #3fbff2;
  text-align: center;
  outline: none;
  text-decoration: none;
  transition: color 0.3s ease-out,
  background-color 0.3s ease-out,
  border-color 0.3s ease-out;
  font-size: 20px;
  border-radius: 3px;
}
.ghost:hover{
background: #3fbff2;
}
</style>
<body>
<div style="overflow:hidden;">
<header class="header" id="header"><!--header-start-->
	<div class="container">
    	<figure class="logo animated fadeInDown delay-07s">
        	<a href="#"><img src="logo.png" alt=""></a>	
        </figure>	
        <h1 class="animated fadeInDown delay-07s">" PUSO NG PAGBABAGO "</h1>
		<span class="we-create animated fadeInUp delay-3s"> <i>-Mayor Rodrigo R. Duterte</i></span><br /><br />
       
    </div>
</div>
</header><!--header-end-->


<nav class="main-nav-outer" id="test"><!--main-nav-start-->
	<div class="container">
        <ul class="main-nav">
        	<li><a href="#header">Home</a></li>
            <li><a href="#service">Sponsors</a></li>
        </ul>
        <a class="res-nav_click" href="#"><i class="fa-bars"></i></a>
    </div>
</nav><!--main-nav-end-->

<section class="main-section paddind" id="Portfolio"><!--main-section-start-->
	<div class="container">
    	<h2>Advocacy</h2>
    	<h6>People's way of support to our beloved Mayor</h6>
        
	</div>
    <div class="portfolioContainer fadeInUp delay-04s">
	<div id="sponsor_data"></div>
          
    </div>
</section><!--main-section-end-->

<section class="business-talking"><!--business-talking-start-->
	<div class="container">
        <h2>Show Your Advocacy.</h2>
    </div>
</section><!--business-talking-end-->
<div class="container">
<section class="main-section contact" id="contact">
	
        <div class="row">
        	<div class="col-lg-6 col-sm-7 wow fadeInLeft">
            	
           
            </div>
        	<div class="col-lg-6 col-sm-5 wow fadeInUp delay-05s">
			<form role="form" id="the_form" enctype="multipart/form-data">
            	<div class="form">
                     <label>Personal Information *</label>
                	<input id="the_fname" class="input-text" type="text" name="" placeholder="First Name" required>
                    <input id="the_mname" class="input-text" type="text" name="" placeholder="Middle Name" required>
                    <input id="the_lname" class="input-text" type="text" name="" placeholder="Last  Name" required>
                    
								
						<label>Gender *</label>
                        <div class="radio">
                            <label>
                                <input type="radio" name="gender" id="optionsRadios1" value="Male" checked />
                                Male
                            </label><br />
                            <label>
                                <input type="radio" name="gender" id="optionsRadios2" value="Female" />
                                Female
                            </label>
                        </div>	
					<label>Contact *</label>
                    <input id="the_contact" class="input-text" type="text" name="" placeholder="Contact" required>
					<label>Email Address *</label>
                    <input id="the_email" class="input-text" type="email" name="" placeholder="E-mail Address" required>
					
                	<label>Address *</label>
					<textarea id="the_address" class="input-text text-area" cols="0" rows="0" placeholder="Address" required></textarea>
                    <label>Company Name *</label>
					<input id="the_company" class="input-text" type="text" name="" placeholder="Company Name" required>
					
					<label>Company Logo *</label>
                    <input class="input-text" type="file" name="myfile" id="the_logo" required>
					
                    <input id="the_register" class="input-btn" type="submit" value="Register">

                </div>	
			</form>
            </div>
        </div>
</section>

<footer class="footer">
    <div class="container">
        <div class="footer-logo"><a href="#"><img src="logo.png" alt=""></a></div>
        <span class="copyright">Copyright © 2016 | Designed by <a href="#">TeamMondestars</a>.</span>
    </div>
    <!-- 
        All links in the footer should remain intact. 
        Licenseing information is available at: http://bootstraptaste.com/license/
        You can buy this theme without footer links online at: http://bootstraptaste.com/buy/?theme=Knight
    -->
</footer>


<script type="text/javascript">
    $(document).ready(function(e) {
 
		$("#the_contact").numeric();
		$("#sponsor_data").load("php/sponsor-data.php");
		
        $('#test').scrollToFixed();
        $('.res-nav_click').click(function(){
            $('.main-nav').slideToggle();
            return false    
            
        });
	
		
		$("#the_register").on('click', function(e){
		var myForm = $('#the_form');
		if (!myForm[0].checkValidity()) {
		}else{
		var fname = $("#the_fname").val();
		var mname = $("#the_mname").val();
		var lname = $("#the_lname").val();
		var email = $("#the_email").val();
		var address = $("#the_address").val();
		var gender = $("input[name=gender]:checked").val();
		var contact = $("#the_contact").val();
		var company = $("#the_company").val();
		var filename = $("#the_logo")[0].files[0]["name"];
		var the_data = "fname="+fname+"&mname="+mname+"&lname="+lname+"&email="+email+"&address="+address+"&gender="+gender+"&contact="+contact+"&company="+company+"&logo="+filename; 
	
			$.ajax({
			type: "POST",
			url: "php/add-supporter.php",
			data: the_data,
			success: function(html){
			alert(html);
			$("#the_fname").val("");
			$("#the_mname").val("");
			$("#the_lname").val("");
			$("#the_region").val("");
			$("#the_city_list").val("");
			$("#the_email").val("");
			$("#the_address").val("");
			$("#the_month").val("");
			$("#the_day").val("");
			$("#the_year").val("");
			$("#the_sector").val("");
			$("#the_contact").val("");
			$("#the_city").val("");
			$("#the_company").val("");
			
			}
			});
			return false;
			}
		});
		
			
			//DRI ANG IEDIT DOY!!!	
			$('#the_register').on('click', function() {
			var file_data = $('#the_logo').prop('files')[0];   
			var form_data = new FormData();                  
			form_data.append('file', file_data);                          
			$.ajax({
                url: 'upload.php', // point to server-side PHP script 
                dataType: 'text',  // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,                         
                type: 'post'
			});
			});
			//DRI MAHUMAN!!
	
    });
</script>


<script type="text/javascript">
	$(window).load(function(){
		
		$('.main-nav li a').bind('click',function(event){
			var $anchor = $(this);
			
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top - 102
			}, 1500,'easeInOutExpo');
			/*
			if you don't want to use the easing effects:
			$('html, body').stop().animate({
				scrollTop: $($anchor.attr('href')).offset().top
			}, 1000);
			*/
			event.preventDefault();
		});
	})
</script>

<script type="text/javascript">

$(window).load(function(){
  
  
  var $container = $('.portfolioContainer'),
      $body = $('body'),
      colW = 375,
      columns = null;

  
  $container.isotope({
    // disable window resizing
    resizable: true,
    masonry: {
      columnWidth: colW
    }
  });
  
  $(window).smartresize(function(){
    // check if columns has changed
    var currentColumns = Math.floor( ( $body.width() -30 ) / colW );
    if ( currentColumns !== columns ) {
      // set new column count
      columns = currentColumns;
      // apply width to container manually, then trigger relayout
      $container.width( columns * colW )
        .isotope('reLayout');
    }
    
  }).smartresize(); // trigger resize to set container width
  $('.portfolioFilter a').click(function(){
        $('.portfolioFilter .current').removeClass('current');
        $(this).addClass('current');
 
        var selector = $(this).attr('data-filter');
        $container.isotope({
			
            filter: selector,
         });
         return false;
    });
  
});

</script>
</body>
</html>