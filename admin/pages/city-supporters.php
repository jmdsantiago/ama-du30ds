<?php
session_start();
include '../php/connection.php';
$uid = $_SESSION['user_id'];
$q = mysql_query("select * from users where u_id='$uid'");
$r = mysql_fetch_array($q,MYSQL_ASSOC);
    $city = $r['r_id'];;
?>
<div class="panel panel-primary">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i>Supporters for <?php echo $city; ?></h3>
    </div>
    <div class="panel-body">
      <div class="row">
        <div class="col-md-12" >
            <div id="response"></div>
        </div>
        <div class="col-md-12" >
        <div class="input-group"> <span class="input-group-addon">Search</span>

            <input id="filter" type="text" class="form-control" placeholder="Type here...">
        </div>
        <table id="shieldui-grid1" class="table table-striped table-hover" >
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Location</th>
                    <th>Contact No.</th>
                    <th>Date Registered</th>
                    <th>Birthday</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody id="myTable" class="searchable">
        <?php
        include '../php/connection.php';
         
       
        $sql = "select * from supporters where city = '$city'";
        $result = mysql_query($sql) or die("Error in Selecting " . mysql_error($connection));
        $x = 1;
        $emparray = array();
        while($row =mysql_fetch_array($result,MYSQL_ASSOC))
        {
            echo "
                <tr>
                    <td>".$row['s_id']."</td>
                    <td>".$row['s_fname']." ".$row['s_mname']." ".$row['s_lname']."</td>
                    <td>".$row['city']." ,".$row['r_id']."</td>
                    <td>".$row['s_contact']."</td>
                    <td>".$row['dateCreated']."</td>
                    <td>".$row['s_bday']."</td>
                    <td><button type='button' class='btn btn-default delete' value='".$row['s_id']."' class='delete-button' >DELETE</button></td>
                </tr>
            ";    
            
        }

        ?>
            </tbody>
        </table>
        
        </div>
        <div class="col-md-12 text-center">
          <ul class="pagination" id="myPager"></ul>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
$("tbody#myTable tr td button").click(function(e){ 
             e.preventDefault();
              var id = $(this).val(); 
            var r = confirm("Are you sure you want to delete the user?");
            if (r == true) {
                 $.ajax({ 
                      type: 'POST',
                         url: "../php/delete-supporter.php",
                         data: "id="+id,
                         success: function(data) { 
                            alert(data);
                             $("#page-wrapper").load("national-supporters.php");
                      } 
                 }); 
            }
                
              
         });

$.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 5,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:5});
    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

           if ($(this).val() == "") {
            $('#myTable').pageMe();
           }

        })

    }(jQuery));



</script>