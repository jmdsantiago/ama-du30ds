<?php

?>
<div class="row">

                <div class="col-md-5">
                    <div id="response"></div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i> Create Coordinator</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form">
                                <input type="hidden" id="uid">
                                <div class="form-group" id="for-fname">
                                    <label class="control-label" for="fname" id="fname-notLetter" hidden>First Name must be only contain alphabet.</label>
                                    <label class="control-label" for="fname" id="fname-null" hidden>First Name should be filled up!</label>
                                    <input type="text" class="form-control" placeholder="First name" id="fname" required>
                                </div>
                                <div class="form-group" id="for-mname">
                                    <label class="control-label" for="mname" id="mname-notLetter" hidden>Middle Name must be only contain alphabet.</label>
                                    <label class="control-label" for="mname" id="mname-null" hidden>Middle Name should be filled up!</label>
                                    <input type="text" class="form-control" placeholder="Middle name" id="mname" required>
                                </div>
                                <div class="form-group" id="for-lname">
                                    <label class="control-label" for="lname" id="lname-notLetter" hidden>Last Name must be only contain alphabet.</label>
                                    <label class="control-label" for="lname" id="lname-null" hidden>Last Name should be filled up!</label>
                                    <input type="text" class="form-control" placeholder="Last name" id="lname" required>
                                </div>
                                
                                 <div class="form-group">
                                    <label>Gender</label>
                                    <div class="radio" id="gender">
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios1" value="male" checked>
                                            Male
                                        </label>
                                        <label>
                                            <input type="radio" name="optionsRadios" id="optionsRadios2" value="female">
                                            Female
                                        </label>
                                    </div>
                                </div>
                                
                                <div class="form-group" id="for-contact">
                                    <label class="control-label" for="contact" id="contact-NaN" hidden>Contact No. must be valid</label>
                                    <label class="control-label" for="contact" id="contact-null" hidden>Contact No. must be filled up!</label>
                                    <input type="text" class="form-control" placeholder="Contact No." id="contact">
                                </div>

                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" rows="3" id="address" required></textarea>
                                </div>
                                <div class="form-group" id="for-region">
                                    <label class="control-label" for="region" id="region-null" hidden>Please select a Region!</label>
                                    <label>Region</label>
                                    <select class="form-control" id="region">
                                        <option value="18">National Capital Region (NCR)</option>
                                        <option value="1">REGION I (Ilocos Region)</option>
                                        <option value="2">REGION II (Cagayan Valley)</option>
                                        <option value="3">REGION III (Central Luzon)</option>
                                        <option value="4">REGION IV-A (CALABARZON)</option>
                                        <option value="5">REGION IV-B (MIMAROPA)</option>
                                        <option value="6">REGION V (Bicol Region)</option>
                                        <option value="7">REGION VI (Western Visayas)</option>
                                        <option value="8">REGION VII (Central Visayas)</option>
                                        <option value="9">REGION VIII (Eastern Visayas)</option>
                                        <option value="10">REGION IX (Zamboanga Peninsula)</option>
                                        <option value="11">REGION X (Northern Mindanao)</option>
                                        <option value="12">REGION XI (Davao Region)</option>
                                        <option value="13">REGION XII (Soccsksargen)</option>
                                        <option value="14">REGION XIII (CARAGA)</option>
                                        <option value="15">REGION XIV Cordillera Administrative Region (CAR)</option>
                                        <option value="16">REGION XV - Autonomous Region in Muslim Mindanao (ARMM)</option>
                                        <option value="17">Region XVIII - NIR - Negros Island Region</option>
                                    </select>
                                </div>
                               <!-- <div class="form-group" id="for-uname">
                                    <label class="control-label" for="uname" id="uname-null" hidden>Kindly provide a Username!</label>
                                    <input type="text" class="form-control" placeholder="Username" id="uname">
                                </div>-->

                            </form>
                        </div>
                        <div class="panel-footer">
                            <button id="register" class="btn btn-success">Register</button>
                            <button id="update" class="btn btn-success">Update</button>
                            <button id="clear" class="btn btn-default">Clear</button>
                            <button id="cancel" class="btn btn-warning">Cancel</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i>Coordinators' Grid</h3>
                        </div>
                        <div class="panel-body">
                            <div id="table-content"></div>
                        </div>
                        <div class="panel-footer">
                            <button  id="refresh-grid" class="btn btn-info">Refresh</button>
                        </div>
                    </div>
                </div>

            </div>
    <script type="text/javascript">
        $('#update').hide();
        $('#cancel').hide();
        
        var fname = $('#fname'), mname = $('#mname'), lname = $('#lname'), address = $('#address'),
            gender = $('#gender'), region = $('#region'), /*uname = $('#uname'),*/ contact = $('#contact');
            $('#table-content').load('../php/coordinator-data.php');


            //validation
            var _fname, _mname, _lname, _address, _gender, _region, /*_uname*/;

           fname.keyup(function (e){ 
                if(!/[a-zA-Z0]/.test(fname.val())){_fname = false; $('#for-fname').addClass('has-error'); $('#fname-notLetter').show(); $('#fname-null').hide(); }else{ _fname = true; $('#for-fname').removeClass('has-error'); $('#fname-null').hide(); $('#fname-notLetter').hide(); }

            });
           mname.keyup(function (e){
               if(!/[a-zA-Z0]/.test(mname.val())){_mname = false; $('#for-mname').addClass('has-error'); $('#mname-notLetter').show(); $('#mname-null').hide();}else{ _mname = true; $('#for-mname').removeClass('has-error'); $('#mname-null').hide(); $('#mname-notLetter').hide(); }

            });
           lname.keyup(function (e){
                if(!/[a-zA-Z0]/.test(lname.val())){_lname = false; $('#for-lname').addClass('has-error'); $('#lname-notLetter').show(); $('#lname-null').hide(); }else{ _lname = true; $('#for-lname').removeClass('has-error'); $('#lname-null').hide(); $('#lname-notLetter').hide(); }

            });
           contact.keyup(function (e){
                if(isNaN(contact.val())){_contact = false; $('#for-contact').addClass('has-error'); $('#contact-NaN').show(); $('#contact-null').hide(); }else{ _contact = true; $('#for-contact').removeClass('has-error'); $('#contact-null').hide(); $('#contact-NaN').hide(); }

            });
            //uname.keyup(function (e){

            //});
            //coordinator registration button triggers
            $('#register').click(function (e){
                e.preventDefault();
                // validates if entries are not empty
                if(fname.val() == null ){_fname = false; $('#for-fname').addClass('has-error'); $('#fname-null').show(); $('#fname-notLetter').hide(); }else{ _fname = true; $('#for-fname').removeClass('has-error'); $('#fname-null').hide(); $('#fname-notLetter').hide(); }
                if(mname.val() == null ){_mname = false; $('#for-mname').addClass('has-error'); $('#mname-null').show(); $('#mname-notLetter').hide();}else{ _mname = true; $('#for-mname').removeClass('has-error'); $('#mname-null').hide(); $('#mname-notLetter').hide(); }
                if(lname.val() == null){_lname = false; $('#for-lname').addClass('has-error'); $('#lname-null').show(); $('#lname-notLetter').hide();}else{ _lname = true; $('#for-lname').removeClass('has-error'); $('#lname-null').hide(); $('#lname-notLetter').hide(); }
                //if(uname.val() == null){_uname = false; $('#for-uname').addClass('has-error'); $('#uname-null').show(); }else{ _uname = true; $('#for-uname').removeClass('has-error'); $('#uname-null').hide();}
                if(region.val() == null){_region = false; $('#for-region').addClass('has-error'); $('#region-null').show(); }else{ _region = true; $('#for-region').removeClass('has-error'); $('#region-null').hide();}
                if(contact.val() == null){_contact = false; $('#for-contact').addClass('has-error'); $('#contact-null').show(); }else{ _contact = true; $('#for-contact').removeClass('has-error'); $('#contact-null').hide();}
                //when all entries are of no errors, send function triggers
                if( _fname == true && _mname == true && _lname == true && _region == true && _uname == true && _contact == true){
                    $.ajax({
                        url: "../php/add-coordinator.php",
                        data: {
                            fname: fname.val(),
                            mname: mname.val(),
                            lname: lname.val(),
                            gender: gender.val(),
                            address: address.val(),
                            region: region.val(),
                            //uname: uname.val(),
                            contact: contact.val()
                        },
                        type: "post",
                        success: function (response){
                            $('#response').html(response);
                            fname.val('');
                            mname.val('');
                            lname.val('');
                            gender.val('');
                            address.val('');
                            region.val('');
                            //uname.val(''); 
                            contact.val('');

                            setTimeout($('#response').html(response),3000); 
                        }

                    });
                }else{
                    alert("Something Went wrong. Please reload the page.");
                }
            });

             $('#update').click(function (e){
                e.preventDefault();
                // validates if entries are not empty
                if(fname.val() == null ){_fname = false; $('#for-fname').addClass('has-error'); $('#fname-null').show(); $('#fname-notLetter').hide(); }else{ _fname = true; $('#for-fname').removeClass('has-error'); $('#fname-null').hide(); $('#fname-notLetter').hide(); }
                if(mname.val() == null ){_mname = false; $('#for-mname').addClass('has-error'); $('#mname-null').show(); $('#mname-notLetter').hide();}else{ _mname = true; $('#for-mname').removeClass('has-error'); $('#mname-null').hide(); $('#mname-notLetter').hide(); }
                if(lname.val() == null){_lname = false; $('#for-lname').addClass('has-error'); $('#lname-null').show(); $('#lname-notLetter').hide();}else{ _lname = true; $('#for-lname').removeClass('has-error'); $('#lname-null').hide(); $('#lname-notLetter').hide(); }
                //if(uname.val() == null){_uname = false; $('#for-uname').addClass('has-error'); $('#uname-null').show(); }else{ _uname = true; $('#for-uname').removeClass('has-error'); $('#uname-null').hide();}
                if(region.val() == null){_region = false; $('#for-region').addClass('has-error'); $('#region-null').show(); }else{ _region = true; $('#for-region').removeClass('has-error'); $('#region-null').hide();}  
                if(contact.val() == null){_contact = false; $('#for-contact').addClass('has-error'); $('#contact-null').show(); }else{ _contact = true; $('#for-contact').removeClass('has-error'); $('#contact-null').hide();}
                //when all entries are of no errors, send function triggers
                if( _fname == true && _mname == true && _lname == true  && _region == true && _uname == true && _contact == true){
                    $.ajax({
                        url: "../php/update-coordinator.php",
                        data: {
                            
                            uid: $('#uid').val(),
                            fname: fname.val(),
                            mname: mname.val(),
                            lname: lname.val(),
                            gender: gender.val(),
                            address: address.val(),
                            region: region.val(),
                            //uname: uname.val(),
                            contact: contact.val()
                        },
                        type: "post",
                        success: function (response){
                            $('#response').html(response);
                            $('#uid').val('')
                            fname.val('');
                            mname.val('');
                            lname.val('');
                            gender.val('');
                            address.val('');
                            
                            //uname.val(''); 
                            contact.val('');

                            $('#register').show();
                            $('#clear').show();
                            $('#update').hide();
                            $('#cancel').hide();

                             $('#table-content').load('../php/coordinator-data.php'); 
                            setTimeout(function(){
                            $('#response').empty();
                            },3000);
                        }

                    });
                }else{
                    alert("Something Went wrong. Please reload the page.");
                }
            });

            $('#clear').click(function (e){
                e.preventDefault();
                fname.val("");
                mname.val("");
                lname.val("");
                address.val("");
                gender.val("");
                region.val("");
                //uname.val("");


            });    
            
            $('#cancel').click(function (e){
                e.preventDefault();
                fname.val("");
                mname.val("");
                lname.val("");
                address.val("");
                gender.val("");
                region.val("");
                //uname.val("");
                
                $('#register').show();
                $('#clear').show();
                $('#update').hide();
                $('#cancel').hide();


            });         



        
    </script>
<?php

?>