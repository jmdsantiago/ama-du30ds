<?php
session_start();
include("../php/connection.php");
?>
<div class="row">

                <div class="col-md-5">
                    <div id="response"></div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i>Change Password</h3>
                        </div>
                        <div class="panel-body">
                            <form role="form" id="myForm">
                                <input type="hidden" id="uid">
                                <div class="form-group" id="for-fname">
								<label>Old Password</label>
                                    <input type="password" class="form-control" placeholder="Type your Old Password here.." id="old_p">
                                </div>
								<label>New Password</label>
                                <div class="form-group" id="for-mname">
                                    <input type="password" class="form-control" placeholder="Type your New Password here.." id="new_p">
                                </div>
								<label>Re-Type New Password</label>
                                <div class="form-group" id="for-mname">
                                    <input type="password" class="form-control" placeholder="RE-Type your New Password here.." id="re_new_p">
                                </div>
                            </form>
                        </div>
                        <div class="panel-footer">
                            <button id="submit" class="btn btn-success">Update</button>
                            <button id="cancel" class="btn btn-warning">Cancel</button>
                        </div>
                    </div>
                </div>


            </div>
    <script type="text/javascript">
            
			$("#submit").click(function(){
			var old_p = $("#old_p").val();
			var new_p = $("#new_p").val();
			var re_new_p = $("#re_new_p").val();
			var data_p = "old_p="+old_p+"&new_p="+new_p;
			if(new_p == re_new_p){
			$.ajax({
				type: "POST",
				url: "../php/change-pass.php",
				data: data_p,
				success: function(html){
					alert(html);
					window.location = "../index.php";
				}
				
			});
			}
			else{
				alert("New Password does not match.");
				$("#new_p").val("");
				$("#re_new_p").val("");
				$("#old_p").val("");
			}
			});
					
					
            $('#cancel').click(function (e){
                e.preventDefault();
                fname.val("");
                mname.val("");
                lname.val("");
                address.val("");
                gender.val("");
                region.val("");
                uname.val("");
                
                $('#register').show();
                $('#clear').show();
                $('#update').hide();
                $('#cancel').hide();


            });         



        
    </script>
<?php

?>