<div class="col-lg-12">
    <h1>Supporter's List<small></small></h1>
</div>
<div class="row">
<div class="col-lg-3">
    <div class="form-group">
        <label>Select a region</label>
        <select class="form-control" id="filter-region">
            <?php
            include '../php/connection.php';
            $query1 = "select * from regions where 1";
            $result1 = mysql_query($query1);
            
            while($row1 = mysql_fetch_array($result1,MYSQL_ASSOC)){
            echo '<option value="'.$row1["r_name"].'"> ('.$row1["r_code"].') '.$row1["r_name"].'</option>';
            }
            ?>
        </select>
    </div>
</div>
<div class="col-lg-3"><div id="city-filter-panel"></div></div>
<div class="col-lg-12"><div id="panel-content"></div></div>
</div>
<script type="text/javascript">
    $('#filter-region').change(function (){
        var key = $('#filter-region').val();
        key = key.replace(/ /g, "%20");
        $('#city-filter-panel').load('city-filter.php?region='+key);
        $('#panel-content').empty();
    });
</script>