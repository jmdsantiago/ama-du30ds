<div class="row">
                <div class="col-lg-12">
                    <h1>Dashboard <small>Statistics and more</small></h1>
                </div>
            </div>
             
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Statistical Overview of Supporters</h3>
                        </div>
                        <div class="panel-body">
                             <div class="row">
                                 <div class="col-lg-3">
                                  <?php
                                  session_start();
                                  include '../php/connection.php';
                                  $uid = $_SESSION['user_id'];
                                  $q = mysql_query("select * from users left join regions on users.r_id=regions.r_id where users.u_id='$uid'");
                                  $r = mysql_fetch_array($q,MYSQL_ASSOC);
                                  $region =  $r['r_name'];
                                  echo "<input type='hidden' value='".$region."' id='region-id'/>";
                                  $result = mysql_query("select * from city_list where city_region like '%$region%'");

                                  echo ' <div class="form-group">
                                          <label>Select City/Municipality</label>
                                          <select id="city" class="form-control">';
                                          echo '<option value="'.$region.'">--REGION WIDE--</option>';
                                  while ($row = mysql_fetch_array($result,MYSQL_ASSOC)) {
                                      echo '<option value="'.$row['city_name'].'">'.$row['city_name'].'</option>';
                                  }
                                  echo '  </select>
                                        </div>';
                                  ?>
                                </div>
                             </div>
                             
                            <div class="row">
                                <div id="chart-content"><img src="../img/loading.gif" id="loading"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 <script type="text/javascript">
jQuery(function ($) {
    $('#loading').hide();
    $('#chart-content').load("admin-regional-charts.php?rid="+ $('#region-id').val().replace(/ /g, "%20"));

     $('#city').on('change', function(){
        $('#loading').show();
        $('#chart-content').empty();
            if($('#city').val() == $('#region-id').val()){
              var key = $('#region-id').val();
               key = key.replace(/ /g, "%20");
              var url = "admin-regional-charts.php?rid="+key;
            
            }else{

             var url = "admin-city-charts.php?cid="+$('#city').val(); //get the link you want to load data from
            }

             $.ajax({ 
                  type: 'GET',
                     url: url,
                     success: function(data) { 
                $('#loading').hide();
                        console.log(data);
                        $("#chart-content").replaceWith("<div id='chart-content'>" +data+ "</div>"); 
                  } 
             }); 
         });


     
        });        

    </script>
