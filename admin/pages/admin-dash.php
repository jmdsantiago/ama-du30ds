<div class="row">
                <div class="col-lg-12">
                    <h1>Dashboard <small>Statistics and more</small></h1>
                </div>
            </div>
             
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Statistical Overview of Supporters</h3>
                        </div>
                        <div class="panel-body">
                             <div class="row">
                                 <div class="col-lg-3">
                                  <div class="form-group" id="for-region">
                                        <label>View reports of:</label>
                                        <select class="form-control" id="region">
                                        <option value="0">--NATION WIDE--</option>
                                        <?php
                                        include '../php/connection.php';
                                        $query1 = "select * from regions where 1";
                                        $result1 = mysql_query($query1); 
                                        ?>

                                        <?php
                                        while($row1 = mysql_fetch_array($result1,MYSQL_ASSOC)){
                                        echo '<option value="'.$row1["r_name"].'"> ('.$row1["r_code"].') '.$row1["r_name"].'</option>';
                                        }
                                        ?>
                                        </select>
                                    </div>
                                </div>
                             </div>
                             
                            <div class="row">
                                <div id="chart-content"><img src="../img/loading.gif" id="loading"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
 <script type="text/javascript">
jQuery(function ($) {
    $('#loading').hide();
    $('#chart-content').load("admin-national-charts.php");

     $('#region').on('change', function(){
        $('#loading').show();
        $('#chart-content').empty();
            if($('#region').val() == 0){
              var url = "admin-national-charts.php";
          
            }else{
              var key = $('#region').val();
              var ss = $('#region').val();
               key = key.replace(/ /g, "%20");
             var url = "admin-regional-charts.php?rid="+key; //get the link you want to load data from
            

             $.ajax({ 
                  type: 'GET',
                     url: url,
                     success: function(data) { 
                $('#loading').hide();
                        console.log(data);
                        $("#chart-content").replaceWith("<div id='chart-content'>"+data+"</div>"); 
                  } 
             }); 
           }
         });


     
        });        

    </script>
