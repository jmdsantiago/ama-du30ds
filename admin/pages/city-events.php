<?php
session_start();
include '../php/connection.php';
$uid = $_SESSION['user_id'];
$query = "select * from users left join city_list on users.r_id=city_list.city_name where users.u_id='$uid'";
$result = mysql_query($query);
$row = mysql_fetch_array($result,MYSQL_ASSOC);
?>
<div class="row">
  <div class="col-lg-12">
      <div id="response"></div>
  </div>
</div>
 <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> Calendar of Events - <?php echo $row['city_name']; ?> City</h3>
            </div>
            <div class="panel-body">
                <div class="row"><div class="col-lg-3">

                </div></div>
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    <!-- Modal -->
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button id="form-x" type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Event Information</h3>
    </div>
    <div class="modal-body">
        <div class="form-group">
             <label>Event Name</label>
             <input type="hidden" id="e_id">
            <input type="text" class="form-control" placeholder="Event Name" id="e_name">
        </div>
        <div class="form-group">
             <label>Description</label>
            <textarea id="e_desc" class="form-control" rows="4"></textarea>
        </div>
		<div class="form-group" id="upload_image">
             <label>Upload Image</label>
             <input type="hidden" id="e_id">
            <input type="file" class="form-control" name="myfile" id="the_logo"required>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="submit">Submit</button>
        <button class="btn btn-info" id="update">Update</button>
        <button class="btn btn-danger" id="delete">Delete</button>
        <button class="btn" id="btn-close">Close</button>
    </div>
  </div></div>
</div>
</div>
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../fullcalendar-2.5.0/lib/moment.min.js"></script>
<script type="text/javascript" src="../fullcalendar-2.5.0/fullcalendar.min.js"></script>
<link rel="stylesheet" type="text/css" href="../fullcalendar-2.5.0/fullcalendar.min.css">
<script>
$('#update').hide();
$('#delete').hide();
 
/*
        date store today date.
        d store today date.
        m store current month.
        y store current year.
      */
      var region = "<?php echo $row['city_name'];?>";
      var old_load_url = "../php/city-events-data.php?cid=<?php echo $row['city_name'];?>";
      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      var data = JSON.stringify(['[<?php
$query = "SELECT ev_id AS \'id\', ev_name AS \'title\', ev_fromDate AS \'start\', ev_toDate AS \'end\' FROM events WHERE 1";
$result = mysql_query($query);
$json = array();
if($result){

while($row = mysql_fetch_array($result,MYSQL_ASSOC)){
    $json = $row;
}
}else{
    $json = null;
}
echo json_encode($json);

?>']);
      /*
        Initialize fullCalendar and store into variable.
        Why in variable?
        Because doing so we can use it inside other function.
        In order to modify its option later.
      */
      
      var calendar = $('#calendar').fullCalendar(
      {
        /*
          header option will define our calendar header.
          left define what will be at left position in calendar
          center define what will be at center position in calendar
          right define what will be at right position in calendar
        */
        header:
        {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        /*
          defaultView option used to define which view to show by default,
          for example we have used agendaWeek.
        */
        defaultView: 'agendaWeek',
        timezone: 'local',
        /*
          selectable:true will enable user to select datetime slot
          selectHelper will add helpers for selectable.
        */
        selectable: true,
        selectHelper: true,
        /*
          when user select timeslot this option code will execute.
          It has three arguments. Start,end and allDay.
          Start means starting time of event.
          End means ending time of event.
          allDay means if events is for entire day or not.
        */
        select: function(start, end, allDay)
        {
          $('#myModal').modal('toggle');
          $('#myModal').modal('show');
		  $('#upload_image').show();

          /*
            after selection user will be promted for enter title for event.
          */
          
          /*
            if title is enterd calendar will add title and event into fullCalendar.
          */
        var dateObj = new Date(start); /* Or empty, for today */
        var dateIntNTZ = dateObj.getTime() - dateObj.getTimezoneOffset() * 60 * 1000;
        var dateObjNTZ = new Date(dateIntNTZ);
        var startDate = dateObjNTZ.toISOString().slice(0, 19).replace('T',' ');

        var dateObj_end = new Date(end); /* Or empty, for today */
        var dateIntNTZ_end = dateObj_end.getTime() - dateObj_end.getTimezoneOffset() * 60 * 1000;
        var dateObjNTZ_end = new Date(dateIntNTZ_end);
        var endDate = dateObjNTZ_end.toISOString().slice(0, 19).replace('T',' ');
          $('#submit').unbind().click(function (e){
            var filename = $("#the_logo")[0].files[0]["name"];
            $.ajax({
                  url: "../php/add-city-events.php",
                  data: {
                      title: $('#e_name').val(),
                      description: $('#e_desc').val(),
                      start: startDate,
                      end: endDate,
                      is_region: $('#e_type').val(),
                      region: region,
                      event_pic: $("#the_logo")[0].files[0]["name"]
                  },
                  type: "post",
                  success: function (response){
                      $('#response').html(response);
                      $('#e_name').val('');
                      $('#e_desc').val('');
                  
                      $('#myModal').modal('hide');

                      $('#calendar').fullCalendar('refetchEvents');
                     
                      setTimeout($('#response').html(response),3000);
                      calendar.fullCalendar('unselect');
                      $('#submit').off("click");
                  }

              });
          });
		  
          $("#submit").on('click', function() {
		  var file_data = $('#the_logo').prop('files')[0];   
		  var form_data = new FormData();                  
		  form_data.append('file', file_data);                          
		  $.ajax({
					url: '../php/upload.php', // point to server-side PHP script 
					dataType: 'text',  // what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,                         
					type: 'post'
		  });
		  });
          
          
        },
        /*
          editable: true allow user to edit events.
        */
        editable: true,
        /*
          events is the main option for calendar.
          for demo we have added predefined events in json object.
        */
        events: {
           url: old_load_url,
           error: function() {
                alert('There was an error while fetching events.');
            }
        },
        eventClick:  function(event, jsEvent, view) {
            //set the values and open the modal
            $('#myModal').modal('toggle');
            $('#myModal').modal('show');
			$('#upload_image').hide();
            $('#submit').hide();
            $('#update').show();
            $('#delete').show();
            $('#e_id').val(event.id);
            $('#e_name').val(event.title);
            $('#e_desc').val(event.description);
            $('#for-type').hide();



            $('#update').unbind().click(function (e){
            $.ajax({
                  url: "../php/update-city-events.php",
                  data: {
                      id: $('#e_id').val(), 
                      title: $('#e_name').val(),
                      description: $('#e_desc').val(),
  
                  },
                  type: "post",
                  success: function (response){
                      $('#response').html(response);
                      $('#e_name').val('');
                      $('#e_desc').val('');
                      
                      $('#region').val('');
                      $('#submit').show();
                      $('#update').hide();
                      $('#delete').hide();
                      $('#myModal').modal('hide');

                      $('#calendar').fullCalendar('refetchEvents');
                     
                      setTimeout($('#response').html(response),3000);
                      calendar.fullCalendar('unselect');

                  }

              });
          });

          $('#delete').unbind().click(function (e){
             
            if(confirm("Are you sure you want to delete "+$('#e_name').val()+" ?") == true){
              $.ajax({
                    url: "../php/delete-city-events.php",
                    data: {
                        id: $('#e_id').val(), 
    
                    },
                    type: "post",
                    success: function (response){
                        $('#response').html(response);
                        $('#e_name').val('');
                        $('#e_desc').val('');
                        
                        $('#region').val('');
                        $('#submit').show();
                        $('#update').hide();
                        $('#delete').hide();
                        $('#myModal').modal('hide');

                        $('#calendar').fullCalendar('refetchEvents');
                       
                        setTimeout($('#response').html(response),3000);
                        calendar.fullCalendar('unselect');

                    }

                });
              }
          });
        }
      });
      
      $('#form-x').click(function(e){
          e.preventDefault;
          $('#e_name').val('');
          $('#e_desc').val('');
          
          $('#submit').show();
          $('#update').hide();
          $('#delete').hide();
          $('#myModal').modal('hide');
      });

      $('#btn-close').click(function(e){
          e.preventDefault;
          $('#e_name').val('');
          $('#e_desc').val('');
          
          $('#submit').show();
          $('#update').hide();
          $('#delete').hide();
          $('#myModal').modal('hide');
      });
      

     
</script>

<?php

?>