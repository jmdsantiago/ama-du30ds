<div id="hello">
<?php 
include '../php/connection.php';
$rid = $_GET['rid'];
$query = "select city ,sum(case when s_suffix = 'Male' then 1 else 0 end) as male, sum(case when s_suffix = 'Female' then 1 else 0 end) as female from supporters where r_id like '%$rid%' AND is_encoded='0' group by city";
$result = mysql_query($query) or die(mysql_error());
$result3 = mysql_query($query) or die(mysql_error());
?>

<div class="row"> 
</div>
<div class="col-lg-2">
<button id="print" class="btn btn-info">PRINT</button>
</div>
<?php 
$q = "select count(s_id) as supporter, sum(case when is_voting = 'YES' then 1 else 0 end) as voter from supporters WHERE r_id like '%$rid%' AND is_encoded='0'";
$r = mysql_query($q);
$w = mysql_fetch_array($r,MYSQL_ASSOC);
?>
<div class="col-lg-12"><h3>Overall Supporters for <?php echo $rid." (".$w['voter']." of ".$w['supporter']." supporters vouch to vote.)";?></h3><div id="shieldui-chart1"></div></div>
<div class="col-lg-4"><div id="shieldui-chart2"></div><h3><center>GENDER</center></h3></div>
<div class="col-lg-4"><div id="shieldui-chart3"></div><h3><center>SECTOR</center></h3></div>
<div class="col-lg-4">
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>CITY</th>
        <th>MALE</th>
        <th>FEMALE</th>
      </tr>
    </thead>
    <tbody>
    <?php
    while($row3 = mysql_fetch_array($result3,MYSQL_ASSOC)){
          echo "<tr><td>".$row3['city']."</td><td>".$row3['male']."</td><td>".$row3['female']."</td></tr>";
      }
    ?>
    </tbody>
  </table><br>
<?php
$q2 = "SELECT sector, count(*) as total FROM `supporters` WHERE r_id like '%$rid%' AND is_encoded='0' GROUP BY sector";
$r2 = mysql_query($q2);

?>
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>SECTOR</th>
        <th>Supporters</th>
      </tr>
    </thead>
    <tbody>
    <?php
    while($w2 = mysql_fetch_array($r2,MYSQL_ASSOC)){
          echo "<tr><td>".$w2['sector']."</td><td>".$w2['total']."</td></tr>";
      }
    ?>
    </tbody>
  </table>
</div>
<div class="col-lg-12"><h3>By Age Bracket</h3><div id="shieldui-chart4"></div></div>
</div>
<div id="editor"></div>
<script type="text/javascript">

  new Morris.Bar({
    element: 'shieldui-chart1',
    data: [ <?php 
      while($row = mysql_fetch_array($result,MYSQL_ASSOC)){
        echo "{'city': '".$row['city']."', 'male': '".$row['male']."', 'female': '".$row['female']."'},";
      }
    ?> ],
    xkey: 'city',
    ykeys: ['male', 'female'],
    labels: ['Male', 'Female']
  });



<?php
$query1 = "select sum(case when s_suffix = 'Male' then 1 else 0 end) as male, sum(case when s_suffix = 'Female' then 1 else 0 end) as female from supporters WHERE r_id like '%$rid%' AND is_encoded='0'";
$result1 = mysql_query($query1);
$row1 = mysql_fetch_array($result1,MYSQL_ASSOC);
$male = $row1['male'];
$female = $row1['female'];
?>
new Morris.Donut({
  element: 'shieldui-chart2',
  data: [ <?php
       
            echo "{ label: 'Male' , value : $male },{ label : 'Female' , value : $female } ";
       
  ?>
  ]
});

<?php
$query2 = "SELECT sector, count(*) as total FROM `supporters` WHERE r_id like '%$rid%' AND is_encoded='0' GROUP BY sector";
$result2 = mysql_query($query2);
?>
new Morris.Donut({
  element: 'shieldui-chart3',
  data: [ <?php
    while($row2 = mysql_fetch_array($result2,MYSQL_ASSOC)){
      echo "{ label : '".$row2['sector']."' , value : '".$row2['total']."' },";
    } 


?>
  ]
});

<?php
$query4 = "SELECT SUM(IF(age BETWEEN 18 and 29,1,0)) as '18 - 29', SUM(IF(age BETWEEN 30 and 39,1,0)) as '30 - 39', SUM(IF(age BETWEEN 40 and 49,1,0)) as '40 - 49', SUM(IF(age BETWEEN 50 and 59,1,0)) as '50 - 59', SUM(IF(age BETWEEN 60 and 69,1,0)) as '60 - 69', SUM(IF(age BETWEEN 70 and 79,1,0)) as '70 - 79', SUM(IF(age >=80, 1, 0)) as 'Over 80' FROM (SELECT TIMESTAMPDIFF(YEAR, s_bday, CURDATE()) AS age FROM supporters WHERE r_id LIKE '%$rid%' AND is_encoded='0') as derived";
$result4 = mysql_query($query4);
$array = array();
while ($row4 = mysql_fetch_array($result4,MYSQL_ASSOC)) {
  $array = $row4;
}
?>

new Morris.Bar({
    element: 'shieldui-chart4',
    data: [ <?php 
      foreach($array as $key => $item){
         echo "{'city': '".$key."', 'age': '".$item."'},";
      }

    ?> ],
    xkey: 'city',
    ykeys: ['age'],
    labels: ['Age Bracket']
  });

$('#print').on('click', function () {
  
   var printContents = document.getElementById('hello').innerHTML;     
   var originalContents = document.body.innerHTML;       
   document.body.innerHTML = printContents;      
   window.print();      
   document.body.innerHTML = originalContents;
   window.location = "";
   
   
});
</script>
