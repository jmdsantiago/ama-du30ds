<?php
session_start();
include("../php/connection.php");
?>
<div class="row">

                <div class="col-md-5">
                    <div id="response"></div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i> Create Supporter</h3>
                            <input type="hidden" id="cooruid" value="<?php echo $_SESSION['user_id']; ?>">
                        </div>
                        <div class="panel-body">
                            <form role="form" id="myForm">
                                <input type="hidden" id="uid">
                                <div class="form-group" id="for-fname">
                                    <label class="control-label" for="fname" id="fname-notLetter" hidden>First Name must be only contain alphabet.</label>
                                    <label class="control-label" for="fname" id="fname-null" hidden>First Name should be filled up!</label>
                                    <input type="text" class="form-control" placeholder="First name" id="fname" required>
                                </div>
                                <div class="form-group" id="for-mname">
                                    <label class="control-label" for="mname" id="mname-notLetter" hidden>Middle Name must be only contain alphabet.</label>
                                    <label class="control-label" for="mname" id="mname-null" hidden>Middle Name should be filled up!</label>
                                    <input type="text" class="form-control" placeholder="Middle name" id="mname" required>
                                </div>
                                <div class="form-group" id="for-lname">
                                    <label class="control-label" for="lname" id="lname-notLetter" hidden>Last Name must be only contain alphabet.</label>
                                    <label class="control-label" for="lname" id="lname-null" hidden>Last Name should be filled up!</label>
                                    <input type="text" class="form-control" placeholder="Last name" id="lname" required>
                                </div>
                                
                                 <div class="form-group">
                                    <label>Gender</label>
                                    <div class="radio" id="gender">
                                        <label>
                                            <input type="radio" name="gender" id="optionsRadios1" value="Male" checked>
                                            Male
                                        </label>
                                        <label>
                                            <input type="radio" name="gender" id="optionsRadios2" value="Female">
                                            Female
                                        </label>
                                    </div>
                                </div>
								
								<label id="birthdd">Birthday</label>
                                <label class="control-label" for="birthday" id="birthday-null" hidden>Birthday</label>
                                <div class="form-group form-inline" id="for-birthday">
									<select class="form-control" id="bmonth">
									<option value="01">January</option>
									<option value="02">February</option>
									<option value="03">March</option>
									<option value="04">April</option>
									<option value="05">May</option>
									<option value="06">June</option>
									<option value="07">July</option>
									<option value="08">August</option>
									<option value="09">September</option>
									<option value="10">October</option>
									<option value="11">November</option>
									<option value="12">December</option>
									</select>
									<select class="form-control" id="bday">
									<?php
									for($x=1; $x<=31; $x++){
									echo '<option>'.$x.'</option>';		
									}
									?>
									</select>
									<select class="form-control" id="byear">
									<?php
									for($x=1900; $x<=2000; $x++){
									echo '<option>'.$x.'</option>';		
									}
									?>
									</select>
                                </div>
                                
                                <div class="form-group" id="for-contact">
								<label>Contact</label>
                                    <label class="control-label" for="contact" id="contact-NaN" hidden>Contact No. must be valid</label>
                                    <label class="control-label" for="contact" id="contact-null" hidden>Contact No. must be filled up!</label>
                                    <input type="text" class="form-control" placeholder="Contact No." id="contact" >
                                </div>
								
                                <div class="form-group" id="for-email">
								<label>Email</label>
                                    <label class="control-label" for="email" id="email-NaN" hidden>Contact No. must be valid</label>
                                    <label class="control-label" for="email" id="email-null" hidden>Contact No. must be filled up!</label>
                                    <input type="email" class="form-control" placeholder="Email Address" id="email">
                                </div>

                                <div class="form-group">
                                    <label>Address</label>
                                    <textarea class="form-control" rows="3" id="address" required></textarea>
                                </div>
								
								<div class="form-group" id="for-region">
                                    <label class="control-label" for="region" id="region-null" hidden>Please select a Region!</label>
                                    <label>City</label> 
									<?php
									$id = $_SESSION['user_id'];
									$sql = mysql_query("select * from users where u_id='$id'");
									$row = mysql_fetch_array($sql);
									echo '<input type="text" value="'.$row['r_id'].'" class="form-control" id="city-region">';
									$city = $row['r_id'];
									$query = mysql_query("select * from city_list where city_name like '%$city%'");
									$fetch = mysql_fetch_array($query);
									echo '<label>Region</label>';
									echo '<input type="text" value="'.$fetch['city_region'].'" class="form-control" id="region">';
									?>
                                    
                                </div>
								
								<div class="form-group" id="for-sector">
								<label>Sector / Company</label>
								<select class="form-control" id="the_sector"> 
								<?php
								$sql = mysql_query("select * from sector_list") or die(mysql_error());
								while($row = mysql_fetch_array($sql)){
									echo '<option>'.$row['sector_name'].'</option> ';
								}
								?>
                                <!-- <select class="form-control" id="the_sector"> 
								<option>Accounting</option> 
								<option>Airlines/Aviation</option> 
								<option>Alternative Dispute Resolution</option> 
								<option>Alternative Medicine</option> 
								<option>Animation</option> 
								<option>Apparel & Fashion</option> 
								<option>Architecture & Planning</option> 
								<option>Arts and Crafts</option> 
								<option>Automotive</option> 
								<option>Aviation & Aerospace</option> 
								<option>Banking</option> 
								<option>Biotechnology</option> <option>Broadcast Media</option> <option>Building Materials</option> <option>Business Supplies and Equipment</option> <option>Capital Markets</option> <option>Chemicals</option> <option>Civic & Social Organization</option> <option>Civil Engineering</option> <option>Commercial Real Estate</option> <option>Computer & Network Security</option> <option>Computer Games</option> <option>Computer Hardware</option> <option>Computer Networking</option> <option>Computer Software</option> <option>Construction</option> <option>Consumer Electronics</option> <option>Consumer Goods</option> <option>Consumer Services</option> <option>Cosmetics</option> <option>Dairy</option> <option>Defense & Space</option> <option>Design</option> <option>Education Management</option> <option>E-Learning</option> <option>Electrical/Electronic Manufacturing</option> <option>Entertainment</option> <option>Environmental Services</option> <option>Events Services</option> <option>Executive Office</option> <option>Facilities Services</option> <option>Farming</option> <option>Financial Services</option> <option>Fine Art</option> <option>Fishery</option> <option>Food & Beverages</option> <option>Food Production</option> <option>Fund-Raising</option> <option>Furniture</option> <option>Gambling & Casinos</option> <option>Glass, Ceramics & Concrete</option> <option>Government Administration</option> <option>Government Relations</option> <option>Graphic Design</option> <option>Health, Wellness and Fitness</option> <option>Higher Education</option> <option>Hospital & Health Care</option> <option>Hospitality</option> <option>Human Resources</option> <option>Import and Export</option> <option>Individual & Family Services</option> <option>Industrial Automation</option> <option>Information Services</option> <option>Information Technology and Services</option> <option>Insurance</option> <option>International Affairs</option> <option>International Trade and Development</option> <option>Internet</option> <option>Investment Banking</option> <option>Investment Management</option> <option>Judiciary</option> <option>Law Enforcement</option> <option>Law Practice</option> <option>Legal Services</option> <option>Legislative Office</option> <option>Leisure, Travel & Tourism</option> <option>Libraries</option> <option>Logistics and Supply Chain</option> <option>Luxury Goods & Jewelry</option> <option>Machinery</option> <option>Management Consulting</option> <option>Maritime</option> <option>Marketing and Advertising</option> <option>Market Research</option> <option>Mechanical or Industrial Engineering</option> <option>Media Production</option> <option>Medical Devices</option> <option>Medical Practice</option> <option>Mental Health Care</option> <option>Military</option> <option>Mining & Metals</option> <option>Motion Pictures and Film</option> <option>Museums and Institutions</option> <option>Music</option> <option>Nanotechnology</option> <option>Newspapers</option> <option>Nonprofit Organization Management</option> <option>Oil & Energy</option> <option>Online Media</option> <option>Outsourcing/Offshoring</option> <option>Package/Freight Delivery</option> <option>Packaging and Containers</option> <option>Paper & Forest Products</option> <option>Performing Arts</option> <option>Pharmaceuticals</option> <option>Philanthropy</option> <option>Photography</option> <option>Plastics</option> <option>Political Organization</option> <option>Primary/Secondary Education</option> <option>Printing</option> <option>Professional Training & Coaching</option> <option>Program Development</option> <option>Public Policy</option> <option>Public Relations and Communications</option> <option>Public Safety</option> <option>Publishing</option> <option>Railroad Manufacture</option> <option>Ranching</option> <option>Real Estate</option> <option>Recreational Facilities and Services</option> <option>Religious Institutions</option> <option>Renewables & Environment</option> <option>Research</option> <option>Restaurants</option> <option>Retail</option> <option>Security and Investigations</option> <option>Semiconductors</option> <option>Shipbuilding</option> <option>Sporting Goods</option> <option>Sports</option> <option>Staffing and Recruiting</option> <option>Supermarkets</option> <option>Telecommunications</option> <option>Textiles</option> <option>Think Tanks</option> <option>Tobacco</option> <option>Translation and Localization</option> <option>Transportation/Trucking/Railroad</option> <option>Utilities</option> <option>Venture Capital & Private Equity</option> <option>Veterinary</option> <option>Warehousing</option> <option>Wholesale</option> <option>Wine and Spirits</option> <option>Wireless</option> <option>Writing and Editing</option>
								</select> -->
								</select>
                                </div>
								<label>Will you participate?</label>
								<div class="radio">
								<label>
                                <input type="radio" name="the_question" id="optionsRadios1" value="Yes" checked />
                                Yes
								</label><br />
								<label>
                                <input type="radio" name="the_question" id="optionsRadios2" value="No" />
                                No
                            </label>
                    </div>	<br />
                            </form>
                        </div>
                        <div class="panel-footer">
                            <input type="submit" id="register" class="btn btn-success" value="Register">
                            <button id="update" class="btn btn-success">Update</button>
                            <button id="clear" class="btn btn-default">Clear</button>
                            <button id="cancel" class="btn btn-warning">Cancel</button>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i>Supporters' Grid</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive" id="table-content"></div>
                        </div>
                    </div>
                </div>

            </div>
    <script type="text/javascript">
        $('#update').hide();
        $('#cancel').hide();
		$("#contact").numeric();
        
        var fname = $('#fname'), mname = $('#mname'), lname = $('#lname'), address = $('#address'),
            gender = $('input[name=gender]:checked'), region = $('#city-region'), contact = $('#contact'), coordinator = $('#city-region');
            $('#table-content').load('../php/supporter-data.php');


            //validation
            var _fname, _mname, _lname, _address, _gender, _region;

           fname.keyup(function (e){ 
                if(!/[a-zA-Z0]/.test(fname.val())){_fname = false; $('#for-fname').addClass('has-error'); $('#fname-notLetter').show(); $('#fname-null').hide(); }else{ _fname = true; $('#for-fname').removeClass('has-error'); $('#fname-null').hide(); $('#fname-notLetter').hide(); }

            });
           mname.keyup(function (e){
               if(!/[a-zA-Z0]/.test(mname.val())){_mname = false; $('#for-mname').addClass('has-error'); $('#mname-notLetter').show(); $('#mname-null').hide();}else{ _mname = true; $('#for-mname').removeClass('has-error'); $('#mname-null').hide(); $('#mname-notLetter').hide(); }

            });
           lname.keyup(function (e){
                if(!/[a-zA-Z0]/.test(lname.val())){_lname = false; $('#for-lname').addClass('has-error'); $('#lname-notLetter').show(); $('#lname-null').hide(); }else{ _lname = true; $('#for-lname').removeClass('has-error'); $('#lname-null').hide(); $('#lname-notLetter').hide(); }

            });
           contact.keyup(function (e){
                if(isNaN(contact.val())){_contact = false; $('#for-contact').addClass('has-error'); $('#contact-NaN').show(); $('#contact-null').hide(); }else{ _contact = true; $('#for-contact').removeClass('has-error'); $('#contact-null').hide(); $('#contact-NaN').hide(); }

            });
            //uname.keyup(function (e){

            //});
            //coordinator registration button triggers
            $('#register').click(function(e){
            var a = document.forms["myForm"]["fname"].value;
			var b = document.forms["myForm"]["mname"].value;
			var c = document.forms["myForm"]["lname"].value;
			var d = document.forms["myForm"]["contact"].value;
			var e = document.forms["myForm"]["city-region"].value;
			var f = document.forms["myForm"]["address"].value;

				//e.preventDefault();
                // validates if entries are not empty
                if(fname.val() == null ){_fname = false; $('#for-fname').addClass('has-error'); $('#fname-null').show(); $('#fname-notLetter').hide(); }else{ _fname = true; $('#for-fname').removeClass('has-error'); $('#fname-null').hide(); $('#fname-notLetter').hide(); }
                if(mname.val() == null ){_mname = false; $('#for-mname').addClass('has-error'); $('#mname-null').show(); $('#mname-notLetter').hide();}else{ _mname = true; $('#for-mname').removeClass('has-error'); $('#mname-null').hide(); $('#mname-notLetter').hide(); }
                if(lname.val() == null){_lname = false; $('#for-lname').addClass('has-error'); $('#lname-null').show(); $('#lname-notLetter').hide();}else{ _lname = true; $('#for-lname').removeClass('has-error'); $('#lname-null').hide(); $('#lname-notLetter').hide(); } 
                 //when all entries are of no errors, send function triggers
                if( _fname == true && _mname == true && _lname == true && _contact == true){
				if (a == null || a == "" || b == null || b == "" || c == null || c == "" || e == null || e == "" || f == null || f == "") {
				alert("Some fields are missing!");
				}else{
                    $.ajax({
                        url: "../php/add-supporter.php",
                        data: {
                            
                            coordinator: coordinator.val(),
                            fname: fname.val(),
                            mname: mname.val(),
                            lname: lname.val(),
                            gender: gender.val(),
                            address: address.val(),
                            contact: contact.val(),
							byear: $("#byear").val(),
							bmonth: $("#bmonth").val(),
							bday: $("#bday").val(),
							email: $("#email").val(),
							region: $("#region").val(),
							sector: $("#the_sector").val(),
							confirmd: $("input[name=the_question]:checked").val()
                        },
                        type: "post",
                        success: function (response){
                            $('#response').html(response);
                            fname.val('');
                            mname.val('');
                            lname.val('');
                            gender.val('');
                            address.val('');
                            contact.val('');

                            setTimeout($('#response').html(response),3000);
                            $('#table-content').load('../php/supporter-data.php'); 
                        }

                    });
				}
                }else{
                    alert("Something Went wrong. Please reload the page.");
                }
			
            });

             $('#update').click(function (e){
                e.preventDefault();
                // validates if entries are not empty
                if(fname.val() == null ){_fname = false; $('#for-fname').addClass('has-error'); $('#fname-null').show(); $('#fname-notLetter').hide(); }else{ _fname = true; $('#for-fname').removeClass('has-error'); $('#fname-null').hide(); $('#fname-notLetter').hide(); }
                if(mname.val() == null ){_mname = false; $('#for-mname').addClass('has-error'); $('#mname-null').show(); $('#mname-notLetter').hide();}else{ _mname = true; $('#for-mname').removeClass('has-error'); $('#mname-null').hide(); $('#mname-notLetter').hide(); }
                if(lname.val() == null){_lname = false; $('#for-lname').addClass('has-error'); $('#lname-null').show(); $('#lname-notLetter').hide();}else{ _lname = true; $('#for-lname').removeClass('has-error'); $('#lname-null').hide(); $('#lname-notLetter').hide(); }
                if(contact.val() == null){_contact = false; $('#for-contact').addClass('has-error'); $('#contact-null').show(); }else{ _contact = true; $('#for-contact').removeClass('has-error'); $('#contact-null').hide();}
                //when all entries are of no errors, send function triggers
                if( _fname == true && _mname == true && _lname == true && _contact == true){
                    $.ajax({
                        url: "../php/update-supporter.php",
                        data: {
                            
                            uid: $('#uid').val(),
                            fname: fname.val(),
                            mname: mname.val(),
                            lname: lname.val(),
                            gender: gender.val(),
                            address: address.val(),
                            contact: contact.val()
                        },
                        type: "post",
                        success: function (response){
                            $('#response').html(response);
                            $('#uid').val('')
                            fname.val('');
                            mname.val('');
                            lname.val('');
                            gender.val('');
                            address.val('');
                            contact.val('');
                            email.val('');

                            $('#register').show();
                            $('#clear').show();
                            $('#update').hide();
                            $('#cancel').hide();
							$('#for-birthday').show();
							$('#birthdd').show()
                            setTimeout($('#response').html(response),3000);
                            $('#table-content').load('../php/supporter-data.php?coor='+coordinator.val()); 
                        }

                    });
                }else{
                    alert("Something Went wrong. Please reload the page.");
                }
            });

            $('#clear').click(function (e){
                e.preventDefault();
                fname.val("");
                mname.val("");
                lname.val("");
                address.val("");
                email.val("");


            });    
            
            $('#cancel').click(function (e){
                e.preventDefault();
                fname.val("");
                mname.val("");
                lname.val("");
                address.val("");
                email.val("");
                contact.val("");
                
                $('#register').show();
                $('#clear').show();
                $('#update').hide();
                $('#cancel').hide();


            });         



        
    </script>
<?php

?>