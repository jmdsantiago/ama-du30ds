<?php
session_start();
include'../php/connection.php';
if( !isset( $_SESSION['user_id'] ))
{
  header('location: ../pages/');
}else{
    $query = "SELECT * FROM users LEFT JOIN levels ON users.l_id=levels.l_id WHERE users.u_id='".$_SESSION['user_id']."'";
    $result = mysql_query($query);
    $row = mysql_fetch_array($result, MYSQL_ASSOC);
    
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Du30DS - <?php echo $row['l_desc']; ?></title>
    
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="../bootstrap/css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="../font-awesome/css/font-awesome.min.css" />
    <link rel="stylesheet" type="text/css" href="../css/fullcalendar.css" />
    <link rel="stylesheet" type="text/css" href="../css/local.css" />

    <script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="../js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="../js/jquery.numeric.min.js"></script>
  
    <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../bootstrap/js/dataTables.bootstrap.min.js"></script>
  

    <!-- you need to include the shieldui css and js assets in order for the charts to work -->
    <link rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/light-bootstrap/all.min.css" />
    <link id="gridcss" rel="stylesheet" type="text/css" href="http://www.shieldui.com/shared/components/latest/css/dark-bootstrap/all.min.css" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <style>
     .charts text{
      fill: white;
      color: white;
     }
    </style>

</head>
<body>
    <div id="wrapper">
          <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.php">Du30DS - <?php echo $row['l_desc']; ?></a>
            </div>
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul id="active" class="nav navbar-nav side-nav">
<?php
    if($row['l_desc'] == "Administrator"){
?>
                    <li class="selected"><a href="admin-dash.php"><i class="fa fa-bullseye"></i> Dashboard</a></li>
                    <li><a href="national-supporters.php"><i class="fa fa-users"></i>Supporters</a></li>
                    <li><a href="national-events.php"><i class="fa fa-tasks"></i> Events</a></li>
            <li><a href="create-regional-coordinator.php"><i class="fa fa-user"></i> Create Regional Coordinator</a></li>
            <li><a href="../php/sponsor-data.php"><i class="fa fa-user"></i> Pending Sponsor</a></li>
            <li><a href="../php/approve-data-sponsor.php"><i class="fa fa-user"></i> Approved Sponsor</a></li>
            <li><a href="../php/unapprove-testimonials.php"><i class="fa fa-user"></i>Pending Testimonial</a></li>
            <li><a href="approve-testimonials.php"><i class="fa fa-user"></i>Approved Testimonial</a></li>			           <li><a href="admin-approve-supporter.php"><i class="fa fa-user"></i> Pending Supporters</a></li>           <li><a href="admin-approved-supporter.php"><i class="fa fa-user"></i> Approved Supporters</a></li> 
<?php
    }else if($row['l_desc'] == "Regional"){
?>
           <li class="selected"><a href="regional-dash.php"><i class="fa fa-bullseye"></i> Dashboard</a></li>
                   <li><a href="regional-supporters.php"><i class="fa fa-users"></i>Supporters</a></li>
                   <li><a href="region-events.php"><i class="fa fa-tasks"></i> Events</a></li>
           <li><a href="create-city-coordinator.php"><i class="fa fa-user"></i> Create Coordinator</a></li>
<?php
    }else if($row['l_desc'] == "Coordinator"){
?>
           <li class="selected"><a href="city-dash.php"><i class="fa fa-bullseye"></i> Dashboard</a></li>
                   <li><a href="city-supporters.php"><i class="fa fa-users"></i>Supporters</a></li>
                   <li><a href="city-events.php"><i class="fa fa-tasks"></i> Events</a></li>
           <li><a href="create-encoder.php"><i class="fa fa-user"></i> Create Encoder</a></li>
           <li><a href="approve-supporter.php"><i class="fa fa-user"></i> Pending Supporters</a></li>
           <li><a href="approved-supporter.php"><i class="fa fa-user"></i> Approved Supporters</a></li>
<?php
    }else if($row['l_desc'] == "Encoder"){
?>  
                    <li><a href="encode-supporter.php"><i class="fa fa-clipboard"></i> Encode Supporter</a></li>
<?php
    }
?>
 <li><a href="change-pass.php" id="change-password" ><i class="fa fa-gear"></i> Change Password</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right navbar-user">
                    <li class="dropdown user-dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $row['u_fname']." ".substr($row['u_mname'],0,1).". ".$row['u_lname']; ?><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><a href="../php/logout.php"><i class="fa fa-power-off"></i> Log Out</a></li>

                        </ul>
                    </li>
                    <li class="divider-vertical"></li>
                    <li>
                        <form class="navbar-search">
                            <input type="text" placeholder="Search" class="form-control">
                        </form>
                    </li>
                </ul>
            </div>
        </nav>

        <div id="page-wrapper">
          

            </div>
           
           
        </div>
    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
jQuery(function ($) {
    $('#loading').hide();

<?php
    if($row['l_desc'] == "Administrator"){
?>
        $('#page-wrapper').load("admin-dash.php");
<?php
    }else if($row['l_desc'] == "Regional"){
?>
    $('#page-wrapper').load("regional-dash.php");
<?php
    }else if($row['l_desc'] == "Coordinator"){
?>
    $('#page-wrapper').load("city-dash.php");
<?php
    }else if($row['l_desc'] == "Encoder"){
?>
    $('#page-wrapper').load("encode-supporter.php");
<?php
    }
?>

     $("ul.side-nav li a").click(function(e){ 
             e.preventDefault();
        $("#page-wrapper").replaceWith("<div id='page-wrapper'><br><br><br><br><br><br> <center><img src='../img/loading.gif' id='loading'></center></div>");
             var url = $(this).attr('href'); //get the link you want to load data from

             $.ajax({ 
                  type: 'GET',
                     url: url,
                     success: function(data) { 
                        $("#page-wrapper").replaceWith("<div id='page-wrapper'>" +data+ "</div>"); 
                  } 
             }); 
         });



        });        
    </script>
</body>
</html>
<?php
}
?>  