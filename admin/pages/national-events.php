<?php

?>
<div class="row">
  <div class="col-lg-12">
      <div id="response"></div>
  </div>
</div>
 <div class="row">
    <div class="col-lg-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-bar-chart-o"></i> NationWide and Regional Events </h3>
            </div>
            <div class="panel-body">
                <div class="row"><div class="col-lg-3">
                <div class="form-group">
                    <label>Region</label>
                    <select class="form-control" id="filter-region">
                     <option value="0">--NATION WIDE--</option>
<?php
include '../php/connection.php';
$query1 = "select * from regions where 1";
$result1 = mysql_query($query1); 
?>
            
<?php
while($row1 = mysql_fetch_array($result1,MYSQL_ASSOC)){
              echo '<option value="'.$row1["r_code"].'"> ('.$row1["r_code"].') '.$row1["r_name"].'</option>';
}
?>
                    </select>
                </div>
                </div></div>
                <div id="calendar"></div>
            </div>
        </div>
    </div>
    <!-- Modal -->
  <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
        <button id="form-x" type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
        <h3 id="myModalLabel">Event Information</h3>
    </div>
    <div class="modal-body">
	<form id="the_form" enctype="multipart/form-data">
        <div class="form-group">
             <label>Event Name</label>
             <input type="hidden" id="e_id">
            <input type="text" class="form-control" placeholder="Event Name" id="e_name">
        </div>
        <div class="form-group">
             <label>Description</label>
            <textarea id="e_desc" class="form-control" rows="4"></textarea>
        </div>
        <div class="form-group" id="for-type">
             <label>Event Type</label>
            <select id="e_type" class="form-control">
              <option value="0">National</option>
              <option value="1">Regional</option>
            </select>
        </div>
        <div class="form-group" id="for-region">
            <label>Region</label>
            <select class="form-control" id="e_region">
<?php
$query = "select * from regions where 1";
$result = mysql_query($query); 
?>
            
<?php
while($row = mysql_fetch_array($result,MYSQL_ASSOC)){
              echo '<option value="'.$row["r_code"].'"> ('.$row["r_code"].') '.$row["r_name"].'</option>';
}
?>
            </select>
        </div>
		<div class="form-group" id="upload_image">
             <label>Upload Image</label>
             <input type="hidden" id="e_id">
            <input type="file" class="form-control" name="myfile" id="the_logo"required>
        </div>
		
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" id="submit">Submit</button>
        <button class="btn btn-info" id="update">Update</button>
        <button class="btn btn-danger" id="delete">Delete</button>
        <button class="btn" id="btn-close">Close</button>
    </div>
	</form>
  </div></div>
</div>
</div>
<script type="text/javascript" src="../js/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="../fullcalendar-2.5.0/lib/moment.min.js"></script>
<script type="text/javascript" src="../fullcalendar-2.5.0/fullcalendar.min.js"></script>
<link rel="stylesheet" type="text/css" href="../fullcalendar-2.5.0/fullcalendar.min.css">
<script>
$('#for-region').hide();
$('#update').hide();
$('#delete').hide();
 
/*
        date store today date.
        d store today date.
        m store current month.
        y store current year.
      */
      var region = "none";
      var old_load_url = "../php/national-events-data.php?rid=none";
      var date = new Date();
      var d = date.getDate();
      var m = date.getMonth();
      var y = date.getFullYear();
      var data = JSON.stringify(['[<?php
$query = "SELECT ev_id AS \'id\', ev_name AS \'title\', ev_fromDate AS \'start\', ev_toDate AS \'end\' FROM events WHERE 1";
$result = mysql_query($query);
$json = array();
if($result){

while($row = mysql_fetch_array($result,MYSQL_ASSOC)){
    $json = $row;
}
}else{
    $json = null;
}
echo json_encode($json);

?>']);
      /*
        Initialize fullCalendar and store into variable.
        Why in variable?
        Because doing so we can use it inside other function.
        In order to modify its option later.
      */
       $('#filter-region').on('change', function (){
          var key = $('#filter-region').val();
          key = key.replace(/ /g, "%20");
         var new_load_url = "../php/national-events-data.php?rid="+key;

         $('#calendar').fullCalendar('removeEventSource', old_load_url);
        $('#calendar').fullCalendar('refetchEvents');
        old_load_url = new_load_url;
        $('#calendar').fullCalendar('addEventSource', old_load_url);
        $('#calendar').fullCalendar('refetchEvents');

      });
      
      var calendar = $('#calendar').fullCalendar(
      {
        /*
          header option will define our calendar header.
          left define what will be at left position in calendar
          center define what will be at center position in calendar
          right define what will be at right position in calendar
        */
        header:
        {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },
        /*
          defaultView option used to define which view to show by default,
          for example we have used agendaWeek.
        */
        defaultView: 'agendaWeek',
        timezone: 'local',
        /*
          selectable:true will enable user to select datetime slot
          selectHelper will add helpers for selectable.
        */
        selectable: true,
        selectHelper: true,
        /*
          when user select timeslot this option code will execute.
          It has three arguments. Start,end and allDay.
          Start means starting time of event.
          End means ending time of event.
          allDay means if events is for entire day or not.
        */
        select: function(start, end, allDay)
        {
          $('#myModal').modal('toggle');
          $('#myModal').modal('show');
		  $('#upload_image').show();
          $('#e_type').on('change', function() {
             // or $(this).val()
            if($('#e_type').val() == "1"){
              $('#for-region').show();
            }else{
              $('#for-region').hide();
              region = "none";
            }
          });
          $('#e_region').on('change', function() {
             // or $(this).val()
              if($('#e_type').val() == "0"){
                  region = "none";
              }else{

              region =  $('#e_region').val();
              }
             
          });

          /*
            after selection user will be promted for enter title for event.
          */
          
          /*
            if title is enterd calendar will add title and event into fullCalendar.
          */
        var dateObj = new Date(start); /* Or empty, for today */
        var dateIntNTZ = dateObj.getTime() - dateObj.getTimezoneOffset() * 60 * 1000;
        var dateObjNTZ = new Date(dateIntNTZ);
        var startDate = dateObjNTZ.toISOString().slice(0, 19).replace('T',' ');

        var dateObj_end = new Date(end); /* Or empty, for today */
        var dateIntNTZ_end = dateObj_end.getTime() - dateObj_end.getTimezoneOffset() * 60 * 1000;
        var dateObjNTZ_end = new Date(dateIntNTZ_end);
        var endDate = dateObjNTZ_end.toISOString().slice(0, 19).replace('T',' ');
          $('#submit').unbind().click(function (e){
          var filename = $("#the_logo")[0].files[0]["name"];
            $.ajax({
                  url: "../php/add-national-events.php",
                  data: {
                      title: $('#e_name').val(),
                      description: $('#e_desc').val(),
                      start: startDate,
                      end: endDate,
                      is_region: $('#e_type').val(),
                      region: region,
                      event_pic: $("#the_logo")[0].files[0]["name"]
                  },
                  type: "post",
                  success: function (response){
                      $('#response').html(response);
                      $('#e_name').val('');
                      $('#e_desc').val('');
                      $('#if-region').hide();
                      $('#region').val('');
                      $('#e_type').val('');
                      $('#myModal').modal('hide');

                      $('#calendar').fullCalendar('refetchEvents');
                     
                      setTimeout($('#response').html(response),3000);
                      calendar.fullCalendar('unselect');
                      $('#submit').off("click");
                  }

              });
          });
		  
          $("#submit").on('click', function() {
		  var file_data = $('#the_logo').prop('files')[0];   
		  var form_data = new FormData();                  
		  form_data.append('file', file_data);                          
		  $.ajax({
					url: '../php/upload.php', // point to server-side PHP script 
					dataType: 'text',  // what to expect back from the PHP script, if anything
					cache: false,
					contentType: false,
					processData: false,
					data: form_data,                         
					type: 'post'
		  });
		  });
          
          
        },
        /*
          editable: true allow user to edit events.
        */
        editable: true,
        /*
          events is the main option for calendar.
          for demo we have added predefined events in json object.
        */
        events: {
           url: old_load_url,
           error: function() {
                alert('There was an error while fetching events.');
            }
        },
        eventClick:  function(event, jsEvent, view) {
            //set the values and open the modal
            $('#myModal').modal('toggle');
            $('#myModal').modal('show');
			$('#upload_image').hide();
            $('#submit').hide();
            $('#update').show();
            $('#delete').show();
            $('#e_id').val(event.id);
            $('#e_name').val(event.title);
            $('#e_desc').val(event.description);
            $('#for-type').hide();



            $('#update').unbind().click(function (e){
            $.ajax({
                  url: "../php/update-national-events.php",
                  data: {
                      id: $('#e_id').val(), 
                      title: $('#e_name').val(),
                      description: $('#e_desc').val(),
  
                  },
                  type: "post",
                  success: function (response){
                      $('#response').html(response);
                      $('#e_name').val('');
                      $('#e_desc').val('');
                      $('#for-type').show();
                      $('#if-region').hide();
                      $('#region').val('');
                      $('#e_type').val('');
                      $('#submit').show();
                      $('#update').hide();
                      $('#delete').hide();
                      $('#myModal').modal('hide');

                      $('#calendar').fullCalendar('refetchEvents');
                     
                      setTimeout($('#response').html(response),3000);
                      calendar.fullCalendar('unselect');

                  }

              });
          });

          $('#delete').unbind().click(function (e){
             
            if(confirm("Are you sure you want to delete "+$('#e_name').val()+" ?") == true){
              $.ajax({
                    url: "../php/delete-national-events.php",
                    data: {
                        id: $('#e_id').val(), 
    
                    },
                    type: "post",
                    success: function (response){
                        $('#response').html(response);
                        $('#e_name').val('');
                        $('#e_desc').val('');
                        $('#for-type').show();
                        $('#if-region').hide();
                        $('#region').val('');
                        $('#e_type').val('');
                        $('#submit').show();
                        $('#update').hide();
                        $('#delete').hide();
                        $('#myModal').modal('hide');

                        $('#calendar').fullCalendar('refetchEvents');
                       
                        setTimeout($('#response').html(response),3000);
                        calendar.fullCalendar('unselect');

                    }

                });
              }
          });
        }
      });
      
      $('#form-x').click(function(e){
          e.preventDefault;
          $('#e_name').val('');
          $('#e_desc').val('');
          $('#for-type').show();
          $('#if-region').hide();
          $('#region').val('');
          $('#e_type').val('');
          $('#submit').show();
          $('#update').hide();
          $('#delete').hide();
          $('#myModal').modal('hide');
      });

      $('#btn-close').click(function(e){
          e.preventDefault;
          $('#e_name').val('');
          $('#e_desc').val('');
          $('#for-type').show();
          $('#if-region').hide();
          $('#region').val('');
          $('#e_type').val('');
          $('#submit').show();
          $('#update').hide();
          $('#delete').hide();
          $('#myModal').modal('hide');
      });
      

     
</script>

<?php

?>