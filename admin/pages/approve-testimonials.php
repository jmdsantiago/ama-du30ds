<?php
session_start();
include("../php/connection.php");
?>
<div class="row">
                <div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i>Approved Testimonials</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive" id="table-content"></div>
                        </div>
                        <div class="panel-footer">
                            <button  id="refresh-grid" class="btn btn-info">Refresh</button>
                        </div>
                    </div>
                </div>

</div>
<script type="text/javascript">
$(document).ready(function(e){
$('#table-content').load('../php/approve-data-testimonials.php');
});
</script>
