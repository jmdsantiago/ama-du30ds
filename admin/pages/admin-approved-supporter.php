<?php
session_start();
include("../php/connection.php");
?>
<div class="row">
                <div>
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"><i class="fa fa-rss"></i>Approved Supporters</h3>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive" id="table-content"></div>
                        </div>
                    </div>
                </div>

</div>
<script type="text/javascript">
$(document).ready(function(e){
$('#table-content').load('../php/admin-approve-data.php'); 
});
</script>
