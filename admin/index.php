<?php
session_start();
if( !isset( $_SESSION['user_id'] ))
{
  

?>
<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>DU30DS Administrator Portal</title>

        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" href="css/form-elements.css">
        <link rel="stylesheet" href="css/style.css">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <!--<link rel="shortcut icon" href="assets/ico/favicon.png">
        <!--<link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">-->
        <!--<link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">-->
        <!--<link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">-->
        <!--<link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">-->
        <style>
        .yy{
        background: #262626;
        }
        </style>
    </head>

    <body>

        <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1><strong>Du30DS Portal</strong> Login Form</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login to our site</h3>
                            		<p>Enter your username and password to log on:</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form"  method="post" class="login-form" action="php/login.php">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>
			                        	<input type="text" name="form-username" placeholder="Username..." class="form-username form-control" id="form-username">
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>
			                        	<input type="password" name="form-password" placeholder="Password..." class="form-password form-control" id="form-password">
			                        </div>
			                        <button type="submit" class="btn" name="go">Sign in!</button>
                                                <a href="" data-toggle="modal" data-target="#forgot_password"><p class="p-container">Forgot Password?</p></a>
			                    </form>
                                <p class="p-container">
                                <?php
                                if(isset($_GET['err'])){
                                    echo '<font color="red"><b>'.$_GET['err'].'</b></font>';
                                }
                                ?>
                                </p>
		                    </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>


    <div class="modal fade" id="forgot_password" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content yy">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Forgot Password? Please Input your Account's Email Address</h4>
        </div>
        <div class="modal-body">
          <label class="pull-left">Email Address: </label>
	  <input type="text" class="form-control" placeholder="Please Input your Email Address" id="email_add" required> <br />
        </div>
        <div class="modal-footer">
		  <button type="button" class="btn btn-primary" id="t_submit">Submit</button>
          <button type="button" class="btn btn-default" data-dismiss="modal" id="t_close">Close</button>
        </div>
      </div>
      
    </div>
	</div>

        <!-- Javascript -->
        <script src="js/jquery-2.1.4.min.js"></script>
        <script src="bootstrap/js/bootstrap.min.js"></script>
        <script src="js/jquery.backstretch.min.js"></script>
        <script src="js/scripts.js"></script>
        
		<script>
		$(document).ready(function(){
		$("#t_submit").click(function(){
		var email = $("#email_add").val();
		if(email == "" || email == null){
		alert("Email Address is empty");
		}else{
		$.ajax({
		url: "php/forgot-password.php",
		type: "POST",
		data: "email="+email,
		success: function(html){
		alert(html);	
		}
		});
		}
		});
		});
		</script>
        <!--[if lt IE 10]>
            <script src="assets/js/placeholder.js"></script>
        <![endif]-->

    </body>

</html>
<?php
}
else
{
     header("Location: pages/index.php");
}
?>	