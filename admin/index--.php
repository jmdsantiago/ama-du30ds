<?php
session_start();
if( !isset( $_SESSION['user_id'] ))
{
  

?>
<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset="UTF-8" /> 
    <title>
        Du30DS Administrator Portal
    </title>
    <link rel="stylesheet" type="text/css" href="style.css" />
</head>
<body>
<form action="php/login.php" method="post">
  <h1>Du30DS Portal<br>Log in</h1>
  <div class="inset">
  <p>
    <label for="email">USERNAME</label>
    <input type="text" name="email" id="email">
  </p>
  <p>
    <label for="password">PASSWORD</label>
    <input type="password" name="password" id="password">
  </p>
  </div>
  <p class="p-container">
    <span>Forgot password ?</span>
    <input type="submit" name="go" id="go" value="Log in">
  </p>
   <p class="p-container">
<?php
if(isset($_GET['err'])){
    echo '<font color="red"><b>'.$_GET['err'].'</b></font>';
}
?>
</p>
</form>
</body>
</html>
<?php
}
else
{
     header("Location: pages/index.php");
}
?>