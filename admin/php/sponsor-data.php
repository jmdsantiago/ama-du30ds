<?php
session_start();

?>
<div class="panel panel-primary">
<div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-rss"></i>Pending Sponsor</h3>
</div>
<div class="panel-body">
<div class="input-group"> <span class="input-group-addon">Search</span>
    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>
<table id="shieldui-grid1" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
			<th>Company</th>
            <th>Contact No.</th>
            <th>Date Registered</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody id="myTable" class="searchable">
<?php
include 'connection.php';

$sql = "select * from sponsor where is_approved='0'";
$result = mysql_query($sql) or die("Error in Selecting " . mysql_error($connection));

$emparray = array();
while($row =mysql_fetch_array($result,MYSQL_ASSOC))
{
    echo "
        <tr>
            <td  id='row-id'>".$row['u_id']."</td>
            <td>".$row['u_fname']." ".$row['u_mname']." ".$row['u_lname']."</td>
            <td>".$row['u_company']."</td>
            <td>".$row['u_contact']."</td>
            <td>".$row['dateCreated']."</td>
			<td><input type='button' class='btn btn-default approve' id='".$row['u_id']."' value='Approve' class='btn btn-default approve'><input type='button' class='btn btn-default delete' id='".$row['u_id']."' value='Delete' class='btn btn-default delete'></td>
        </tr>
    ";    
}

?>
    </tbody>
</table>
<div class="col-md-12 text-center">
  <ul class="pagination" id="myPager"></ul>
</div>
</div>
</div>
 <script src="js/jquery.dataTables.min.js"></script>
 <script >
           
           $('#shieldui-grid1 tbody tr ').on('click', 'td#row-id' ,function () {
              var data = $(this).html();
              console.log(data);
              $.ajax({
                url: "../php/supporter-table-data.php", 
                type: "get", 
                data: "uid="+data,
                dataType:'json', 
                success: function(result){
                  var user = result;
                  
                  $('#uid').val(user.u_id);
                  $('#fname').val(user.u_fname);
                  $('#mname').val(user.u_mname);
                  $('#lname').val(user.u_lname);
                  $('#contact').val(user.u_contact);
                  $('#address').val(user.u_address);
                  $('#uname').val(user.u_uname);
               
                  $('#update').show();
                  $('#cancel').show();
                  $('#register').hide();
                  $('#clear').hide();
				   $('#for-birthday').hide();
				  $('#birthdd').hide();
                }
            });
          });
		  
		   $(".delete").click(function(){
			var txt;
			var r = confirm("Are you sure you want to delete the user?");
			var the_id = this.id;
			if (r == true) {
			$.ajax({
				url: "../php/delete-sponsor.php",
				type: "POST",
				data: "id="+the_id,
				success: function(html){
				alert(html);
				$('#page-wrapper').load('../php/sponsor-data.php'); 
				}
			});
			return false;
			} else {
			txt = "Deletion Canceled!";
			}
			alert(txt);
		  });
		  
		   $(".approve").click(function(){
			var txt;
			var r = confirm("Are you sure you want to APPROVE the sponsor?");
			var the_id = this.id;
			if (r == true) {
			$.ajax({
				url: "../php/approve-sponsor.php",
				type: "POST",
				data: "id="+the_id,
				success: function(html){
				alert(html);
				//$('#shieldui-grid1').load('unapprove-testimonials.php'); 
				location.reload();
				}
			});
			return false;
			} else {
			txt = "Canceled!";
			}
			alert(txt);
			});

// ===========================================UPDATED SCRIPTS============================================
$("tbody#myTable tr td button").click(function(e){ 
             e.preventDefault();
              var id = $(this).val(); 
            var r = confirm("Are you sure you want to delete the user?");
            if (r == true) {
                 $.ajax({ 
                      type: 'POST',
                         url: "../php/delete-supporter.php",
                         data: "id="+id,
                         success: function(data) { 
                            alert(data);
                             $("#page-wrapper").load("national-supporters.php");
                      } 
                 }); 
            }
                
              
         });

$.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 5,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:5});
    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

           if ($(this).val() == "") {
            $('#myTable').pageMe();
           }

        })

    }(jQuery));
// ===========================================END OF UPDATED SCRIPTS============================================
        </script>