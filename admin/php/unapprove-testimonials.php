<?php
session_start();

?>
<div class="panel panel-primary">
<div class="panel-heading">
	<h3 class="panel-title"><i class="fa fa-rss"></i>Pending Testimonials</h3>
</div>
<div class="panel-body">
<div class="input-group"> <span class="input-group-addon">Search</span>
    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>
<table id="shieldui-grid1" class="table table-responsive table-striped table-hover">
    <thead>
        <tr>
            <th>Action</th>
            <th>Full Name</th>
            <th>Testimonials</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody id="myTable" class="searchable">
<?php
include 'connection.php';
$sql = "select * from testimonials where is_approved='0'";
$result = mysql_query($sql) or die("Error in Selecting " . mysql_error($connection));

$emparray = array();
while($row =mysql_fetch_array($result,MYSQL_ASSOC))
{
    echo "
        <tr>
            <td><input type='checkbox' name='approve-me[]' value=".$row['t_id']." class='the-approve'></td>
            <td>".$row['fullname']."</td>
            <td>".$row['t_testimonial']."</td>
			<td><input type='button' class='btn btn-default approve' id='".$row['t_id']."' value='Approve' class='btn btn-default approve'> &nbsp <input type='button' class='btn btn-default delete' id='".$row['t_id']."' value='Delete' class='btn btn-default delete'></td>
			
        </tr>
    ";    
}
?>
    </tbody>
</table>
<div class="col-md-12 text-center">
  <ul class="pagination" id="myPager"></ul>
</div>
</div>
<div class="panel-footer">
    <button  id="approve-checked" class="btn btn-info">Approve Selected</button>
    <button  id="approve-all" class="btn btn-info">Approve All</button>
							
</div>
</div>



 <script src="js/jquery.dataTables.min.js"></script>
 <script >
		   $(".delete").click(function(){
			var txt;
			var r = confirm("Are you sure you want to DELETE the user?");
			var the_id = this.id;
			if (r == true) {
			$.ajax({
				url: "../php/delete-testimonials.php",
				type: "POST",
				data: "id="+the_id,
				success: function(html){
				alert(html);
				//$('#shieldui-grid1').load('../php/unapprove-testimonials.php'); 
				location.reload();
				}
			});
			return false;
			} else {
			txt = "Canceled!";
			}
			alert(txt);
		  });
		  
		  
		   $(".approve").click(function(){
			var txt;
			var r = confirm("Are you sure you want to APPROVE the supporter?");
			var the_id = this.id;
			if (r == true) {
			$.ajax({
				url: "../php/approve-testimonials.php",
				type: "POST",
				data: "id="+the_id,
				success: function(html){
				alert(html);
				//$('#shieldui-grid1').load('unapprove-testimonials.php'); 
				location.reload();
				}
			});
			return false;
			} else {
			txt = "Canceled!";
			}
			alert(txt);
			});
			
			$("#approve-checked").click(function(){
			var the_datas = $('input[type="checkbox"]:checked').serialize();
			$.ajax({
			url: "../php/approve-selected-testimonials.php",
			type: "GET",
			data: the_datas,
			success: function(html){
				alert(html);
				//$('#shieldui-grid1').load('unapprove-testimonials.php'); 
				location.reload();
			}
				
			});
			return false;
			});
			
			$("#approve-all").click(function(){
			var txtz;
			var cc = $("#city_the").val();
			var dataa = "city="+cc;
			var z = confirm("Are you sure you want to APPROVE ALL the supporter?");
			if (z == true) {
				$.ajax({
				url: "../php/approve-all-testimonials.php",
				type: "POST",
				data: dataa,
				success: function(html){
				alert(html);
				//$('#shieldui-grid1').load('../php/unapprove-testimonials.php'); 
				location.reload();
				}
				});
				return false;
				} else {
				txtz = "Canceled!";
				}
				alert(txtz);
			});
			
			// ===========================================UPDATED SCRIPTS============================================
$("tbody#myTable tr td button").click(function(e){ 
             e.preventDefault();
              var id = $(this).val(); 
            var r = confirm("Are you sure you want to delete the user?");
            if (r == true) {
                 $.ajax({ 
                      type: 'POST',
                         url: "../php/delete-supporter.php",
                         data: "id="+id,
                         success: function(data) { 
                            alert(data);
                             $("#page-wrapper").load("national-supporters.php");
                      } 
                 }); 
            }
                
              
         });

$.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 5,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:5});
    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

           if ($(this).val() == "") {
            $('#myTable').pageMe();
           }

        })

    }(jQuery));
// ===========================================END OF UPDATED SCRIPTS============================================
        </script>