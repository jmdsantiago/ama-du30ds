<div class="input-group"> <span class="input-group-addon">Search</span>
    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>
<table id="shieldui-grid1" class="table table-striped table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>City</th>
            <th>Contact No.</th>
            <th>Date Registered</th>
            <th>Birthday</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody id="myTable" class="searchable">
<?php
include 'connection.php';

$sql = mysql_query("select * from users where l_id = '3'");
/*$sql = "select users.u_id as 'ID', concat(concat(concat(concat( users.u_fname, ' '), users.u_mname), ' '), users.u_lname) as 'Name', regions.r_code as 'Region', users.u_contact as 'Contact_No.', users.dateCreated as 'Date_Registered' from users 
LEFT JOIN regions ON users.r_id=regions.r_id where users.l_id = 2";*/
//$result = mysql_query($sql) or die("Error in Selecting " . mysql_error($connection));

//$emparray = array();
//DELETE ANG KULANG PARA SA COORDINATOR
while($row =mysql_fetch_array($sql))
{
    echo "
        <tr>
            <td id='row-id'>".$row['u_id']."</td>
            <td>".$row['u_fname']." ".$row['u_mname']." ".$row['u_lname']."</td>
            <td>".$row['r_id']."</td>
            <td>".$row['u_contact']."</td>
            <td>".$row['dateCreated']."</td>
            <td>".$row['u_bday']."</td>
            <td><input type='button' id='".$row['u_id']."' value='Delete' class='btn btn-default delete' name=''></td>
        </tr>
    ";    
}
?>
    </tbody>
</table>
<div class="col-md-12 text-center">
  <ul class="pagination" id="myPager"></ul>
</div>
 <script src="js/jquery.dataTables.min.js"></script>
 <script >
           $('#shieldui-grid1 tbody tr ').on('click', 'td#row-id' ,function () {
              var data = $(this).html();
              console.log(data);
              $.ajax({
                url: "../php/city-coordinator-table-data.php", 
                type: "get", 
                data: "uid="+data, 
		dataType:'json',
                success: function(result){
                  var user = result;
                  $('#uid').val(user.u_id);
                  $('#fname').val(user.u_fname);
                  $('#mname').val(user.u_mname);
                  $('#lname').val(user.u_lname);
                  $('#contact').val(user.u_contact);
                  $('#address').val(user.u_address);
                  $('#the_email').val(user.u_email);
				  $('#region').val(user.r_id);
                  $('#update').show();
                  $('#cancel').show();
                  $('#register').hide();
                  $('#clear').hide();
				  $('#for-birthday').hide();
				  $('#birthdd').hide();
                }
            });
          });

		  $(".delete").click(function(){
			var txt;
			var r = confirm("Are you sure you want to delete the user?");
			var the_id = this.id;
			if (r == true) {
			$.ajax({
				url: "../php/delete.php",
				type: "POST",
				data: "id="+the_id,
				success: function(html){
				alert(html);
				$('#table-content').load('../php/city-coordinator-data.php'); 
				}
			});
			return false;
			} else {
			txt = "Deletion Canceled!";
			}
			alert(txt);
		  });

// ===========================================UPDATED SCRIPTS============================================
$("tbody#myTable tr td button").click(function(e){ 
             e.preventDefault();
              var id = $(this).val(); 
            var r = confirm("Are you sure you want to delete the user?");
            if (r == true) {
                 $.ajax({ 
                      type: 'POST',
                         url: "../php/delete-supporter.php",
                         data: "id="+id,
                         success: function(data) { 
                            alert(data);
                             $("#page-wrapper").load("national-supporters.php");
                      } 
                 }); 
            }
                
              
         });

$.fn.pageMe = function(opts){
    var $this = this,
        defaults = {
            perPage: 5,
            showPrevNext: false,
            hidePageNumbers: false
        },
        settings = $.extend(defaults, opts);
    
    var listElement = $this;
    var perPage = settings.perPage; 
    var children = listElement.children();
    var pager = $('.pager');
    
    if (typeof settings.childSelector!="undefined") {
        children = listElement.find(settings.childSelector);
    }
    
    if (typeof settings.pagerSelector!="undefined") {
        pager = $(settings.pagerSelector);
    }
    
    var numItems = children.size();
    var numPages = Math.ceil(numItems/perPage);

    pager.data("curr",0);
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="prev_link">«</a></li>').appendTo(pager);
    }
    
    var curr = 0;
    while(numPages > curr && (settings.hidePageNumbers==false)){
        $('<li><a href="#" class="page_link">'+(curr+1)+'</a></li>').appendTo(pager);
        curr++;
    }
    
    if (settings.showPrevNext){
        $('<li><a href="#" class="next_link">»</a></li>').appendTo(pager);
    }
    
    pager.find('.page_link:first').addClass('active');
    pager.find('.prev_link').hide();
    if (numPages<=1) {
        pager.find('.next_link').hide();
    }
    pager.children().eq(1).addClass("active");
    
    children.hide();
    children.slice(0, perPage).show();
    
    pager.find('li .page_link').click(function(){
        var clickedPage = $(this).html().valueOf()-1;
        goTo(clickedPage,perPage);
        return false;
    });
    pager.find('li .prev_link').click(function(){
        previous();
        return false;
    });
    pager.find('li .next_link').click(function(){
        next();
        return false;
    });
    
    function previous(){
        var goToPage = parseInt(pager.data("curr")) - 1;
        goTo(goToPage);
    }
     
    function next(){
        goToPage = parseInt(pager.data("curr")) + 1;
        goTo(goToPage);
    }
    
    function goTo(page){
        var startAt = page * perPage,
            endOn = startAt + perPage;
        
        children.css('display','none').slice(startAt, endOn).show();
        
        if (page>=1) {
            pager.find('.prev_link').show();
        }
        else {
            pager.find('.prev_link').hide();
        }
        
        if (page<(numPages-1)) {
            pager.find('.next_link').show();
        }
        else {
            pager.find('.next_link').hide();
        }
        
        pager.data("curr",page);
        pager.children().removeClass("active");
        pager.children().eq(page+1).addClass("active");
    
    }
};

    
  $('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:5});
    (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

           if ($(this).val() == "") {
            $('#myTable').pageMe();
           }

        })

    }(jQuery));
// ===========================================END OF UPDATED SCRIPTS============================================
        </script>